﻿namespace EnphaseParserTest
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.updateButton = new System.Windows.Forms.ToolStripButton();
			this.urlTextBox = new System.Windows.Forms.ToolStripTextBox();
			this.outputTextbox = new System.Windows.Forms.RichTextBox();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.urlTextBox,
            this.updateButton});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip1.Size = new System.Drawing.Size(754, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// updateButton
			// 
			this.updateButton.Image = ((System.Drawing.Image)(resources.GetObject("updateButton.Image")));
			this.updateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.updateButton.Name = "updateButton";
			this.updateButton.Size = new System.Drawing.Size(72, 22);
			this.updateButton.Text = "Get Data";
			this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
			// 
			// urlTextBox
			// 
			this.urlTextBox.Name = "urlTextBox";
			this.urlTextBox.Size = new System.Drawing.Size(200, 25);
			this.urlTextBox.Text = global::EnphaseParserTest.Properties.Settings.Default.ServerURL;
			// 
			// outputTextbox
			// 
			this.outputTextbox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.outputTextbox.Location = new System.Drawing.Point(0, 25);
			this.outputTextbox.Name = "outputTextbox";
			this.outputTextbox.Size = new System.Drawing.Size(754, 390);
			this.outputTextbox.TabIndex = 2;
			this.outputTextbox.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(754, 415);
			this.Controls.Add(this.outputTextbox);
			this.Controls.Add(this.toolStrip1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripTextBox urlTextBox;
		private System.Windows.Forms.ToolStripButton updateButton;
		private System.Windows.Forms.RichTextBox outputTextbox;
	}
}

