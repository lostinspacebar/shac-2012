﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnphaseParserTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void updateButton_Click(object sender, EventArgs e)
		{
			EnphaseParser.EnphaseDataClient client = new EnphaseParser.EnphaseDataClient(this.urlTextBox.Text);
			client.Update(false, true, false);

			string output = "";
			output += "===================== Production ====================\r\n" +
					  "Current = " + client.Production.Current + " W\r\n" +
					  "Today = " + client.Production.Today + " W\r\n" +
					  "This week = " + client.Production.PastWeek + " W\r\n" +
					  "Since Install = " + client.Production.SinceInstallation + " W\r\n\r\n";

			outputTextbox.Text = output;
		}
	}
}
