﻿namespace LinkTest
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.selectSerialPortCombo = new System.Windows.Forms.ToolStripComboBox();
			this.connectButton = new System.Windows.Forms.ToolStripButton();
			this.disconnectButton = new System.Windows.Forms.ToolStripButton();
			this.mainSplit = new System.Windows.Forms.SplitContainer();
			this.logTextbox = new System.Windows.Forms.RichTextBox();
			this.commandInputTextbox = new System.Windows.Forms.TextBox();
			this.updateTimer = new System.Windows.Forms.Timer(this.components);
			this.toolStrip1.SuspendLayout();
			this.mainSplit.Panel2.SuspendLayout();
			this.mainSplit.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.AutoSize = false;
			this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectSerialPortCombo,
            this.connectButton,
            this.disconnectButton});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Padding = new System.Windows.Forms.Padding(4, 4, 1, 4);
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip1.Size = new System.Drawing.Size(541, 35);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// selectSerialPortCombo
			// 
			this.selectSerialPortCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.selectSerialPortCombo.Name = "selectSerialPortCombo";
			this.selectSerialPortCombo.Size = new System.Drawing.Size(121, 27);
			this.selectSerialPortCombo.DropDown += new System.EventHandler(this.selectSerialPortCombo_DropDown);
			// 
			// connectButton
			// 
			this.connectButton.Image = ((System.Drawing.Image)(resources.GetObject("connectButton.Image")));
			this.connectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.connectButton.Name = "connectButton";
			this.connectButton.Size = new System.Drawing.Size(72, 24);
			this.connectButton.Text = "Connect";
			this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
			// 
			// disconnectButton
			// 
			this.disconnectButton.Enabled = false;
			this.disconnectButton.Image = ((System.Drawing.Image)(resources.GetObject("disconnectButton.Image")));
			this.disconnectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.disconnectButton.Name = "disconnectButton";
			this.disconnectButton.Size = new System.Drawing.Size(86, 24);
			this.disconnectButton.Text = "Disconnect";
			this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
			// 
			// mainSplit
			// 
			this.mainSplit.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplit.Location = new System.Drawing.Point(0, 35);
			this.mainSplit.Name = "mainSplit";
			this.mainSplit.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// mainSplit.Panel1
			// 
			this.mainSplit.Panel1.BackColor = System.Drawing.Color.White;
			// 
			// mainSplit.Panel2
			// 
			this.mainSplit.Panel2.Controls.Add(this.logTextbox);
			this.mainSplit.Panel2.Controls.Add(this.commandInputTextbox);
			this.mainSplit.Size = new System.Drawing.Size(541, 453);
			this.mainSplit.SplitterDistance = 310;
			this.mainSplit.SplitterWidth = 7;
			this.mainSplit.TabIndex = 1;
			// 
			// logTextbox
			// 
			this.logTextbox.BackColor = System.Drawing.Color.White;
			this.logTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.logTextbox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.logTextbox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.logTextbox.Location = new System.Drawing.Point(0, 25);
			this.logTextbox.Name = "logTextbox";
			this.logTextbox.ReadOnly = true;
			this.logTextbox.Size = new System.Drawing.Size(541, 111);
			this.logTextbox.TabIndex = 2;
			this.logTextbox.Text = "";
			// 
			// commandInputTextbox
			// 
			this.commandInputTextbox.Dock = System.Windows.Forms.DockStyle.Top;
			this.commandInputTextbox.Location = new System.Drawing.Point(0, 0);
			this.commandInputTextbox.Name = "commandInputTextbox";
			this.commandInputTextbox.Size = new System.Drawing.Size(541, 25);
			this.commandInputTextbox.TabIndex = 3;
			this.commandInputTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandInputTextbox_KeyDown);
			// 
			// updateTimer
			// 
			this.updateTimer.Interval = 500;
			this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(541, 488);
			this.Controls.Add(this.mainSplit);
			this.Controls.Add(this.toolStrip1);
			this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "MainWindow";
			this.Text = "SHAC Data Link Tester";
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.mainSplit.Panel2.ResumeLayout(false);
			this.mainSplit.Panel2.PerformLayout();
			this.mainSplit.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripComboBox selectSerialPortCombo;
		private System.Windows.Forms.ToolStripButton connectButton;
		private System.Windows.Forms.SplitContainer mainSplit;
		private System.Windows.Forms.RichTextBox logTextbox;
		private System.Windows.Forms.TextBox commandInputTextbox;
		private System.Windows.Forms.ToolStripButton disconnectButton;
		private System.Windows.Forms.Timer updateTimer;
	}
}

