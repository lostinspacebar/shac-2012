﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace LinkTest
{
	public partial class MainWindow : Form
	{
		SerialPort port = null;

		/// <summary>
		/// Constructor
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Fill in serial port list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void selectSerialPortCombo_DropDown(object sender, EventArgs e)
		{
			string[] ports = SerialPort.GetPortNames();
			foreach (string port in ports)
			{
				this.selectSerialPortCombo.Items.Add(port);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void connectButton_Click(object sender, EventArgs e)
		{
			this.port = new SerialPort(this.selectSerialPortCombo.SelectedItem.ToString(), 9600);
			this.port.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceived);
			this.port.Open();

			this.connectButton.Enabled = false;
			this.disconnectButton.Enabled = true;
			this.updateTimer.Enabled = true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void disconnectButton_Click(object sender, EventArgs e)
		{
			this.port.Close();
			this.port.Dispose();
			this.port = null;

			this.updateTimer.Enabled = false;
			this.connectButton.Enabled = true;
			this.disconnectButton.Enabled = false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(delegate()
				{
					string line = this.port.ReadLine();
					if (line == "")
					{
						line = "<EMPTY>";
						return;
					}
					this.logTextbox.AppendText(line);
					this.logTextbox.SelectionStart = this.logTextbox.TextLength;
					this.logTextbox.ScrollToCaret();
				}));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void commandInputTextbox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				this.port.Write(this.commandInputTextbox.Text + "\r");
			}
		}

		private void updateTimer_Tick(object sender, EventArgs e)
		{
			this.port.Write("@01 AI 0\r");
		}
	}
}
