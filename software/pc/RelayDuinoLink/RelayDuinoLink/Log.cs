﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHACDataLink
{
	public class Log
	{

		

		/// <summary>
		/// Delegate for log messages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void MessageEventHandler(object sender, MessageEventArgs e);

		/// <summary>
		/// Event data for log messages
		/// </summary>
		public class MessageEventArgs
		{
			/// <summary>
			/// Gets the contents of the log message
			/// </summary>
			public string Message
			{
				get;
				private set;
			}

			/// <summary>
			/// Gets date/time stamp for the message
			/// </summary>
			public DateTime Timestamp
			{
				get;
				private set;
			}

			/// <summary>
			/// Gets any associated exception
			/// </summary>
			public Exception Exception
			{
				get;
				private set;
			}
		}

	}
}
