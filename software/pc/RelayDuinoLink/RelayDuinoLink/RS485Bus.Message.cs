﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHACDataLink
{
	public partial class RS485Bus
	{

		/// <summary>
		/// Delegate for message events on the rs485 bus
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void MessageReceivedEventHandler(object sender, MessageReceivedEventArgs e);

		/// <summary>
		/// Event data for a bus message event
		/// </summary>
		public class MessageReceivedEventArgs
		{
			/// <summary>
			/// Gets the adress this message was meant for
			/// </summary>
			public int Address
			{
				get;
				private set;
			}

			/// <summary>
			/// Gets the contents of the message received over the bus
			/// </summary>
			public string Message
			{
				get;
				private set;
			}

			/// <summary>
			/// Gets date/time stamp for when this message was received
			/// </summary>
			public DateTime Timestamp
			{
				get;
				private set;
			}

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="address"></param>
			/// <param name="message"></param>
			/// <param name="timestamp"></param>
			public MessageReceivedEventArgs(int address, string message, DateTime timestamp)
			{
				this.Address = address;
				this.Message = message;
				this.Timestamp = timestamp;
			}
		}
	}
}
