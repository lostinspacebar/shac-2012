﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace SHACDataLink
{
	public partial class RS485Bus
	{

		/// <summary>
		/// Event raised when a message is received over the bus
		/// </summary>
		public event MessageReceivedEventHandler MessageReceived = delegate { };

		/// <summary>
		/// Static instance
		/// </summary>
		public static RS485Bus Instance
		{
			get;
			private set;
		}

		/// <summary>
		/// Serial port to communicate to the first RS485 device in the chain with
		/// </summary>
		private SerialPort serialPort;

		/// <summary>
		/// Current message buffer
		/// </summary>
		private StringBuilder currentMessage = new StringBuilder(500);

		/// <summary>
		/// Intializes the common RS485 connection
		/// </summary>
		/// <param name="serialPort">Name of serial port that's connected to the first RS485 device in the chain</param>
		/// <param name="baudRate"></param>
		public static void Initialize(string serialPort, int baudRate)
		{
			RS485Bus.Instance = new RS485Bus(serialPort, baudRate);
		}

		/// <summary>
		/// Write a message to the specified address
		/// </summary>
		/// <param name="address"></param>
		/// <param name="message"></param>
		public void SendMessage(int address, string message)
		{
			// Append address and newline to message
			message = "@" + address.ToString("00") + message;

			// Send it over ze serial port
			this.serialPort.Write(message);
		}

		/// <summary>
		/// Ninja Constructor
		/// </summary>
		private RS485Bus(string serialPort, int baudRate)
		{
			this.serialPort = new SerialPort(serialPort, baudRate);
			this.serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceived);
		}

		/// <summary>
		/// Handle data over the serial port
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			while (this.serialPort.BytesToRead > 0)
			{
				char c = Convert.ToChar(this.serialPort.ReadChar());
				if (c == '\n')
				{
					// Message done. Process.
					this.ProcessMessage(this.currentMessage.ToString());
				}
				else
				{
					// Add to current message
					this.currentMessage.Append(c);
				}
			}
		}

		/// <summary>
		/// Process message over bus
		/// </summary>
		/// <param name="p"></param>
		private void ProcessMessage(string message)
		{
			try
			{
				// Parse address
				int address = int.Parse(message.Substring(1, 3));

				// Send out event
				this.MessageReceived(this, new MessageReceivedEventArgs(address, message.Substring(2), DateTime.Now));
			}
			catch(Exception ex)
			{
				
			}
		}
	}
}
