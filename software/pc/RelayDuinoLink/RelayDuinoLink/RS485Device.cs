﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHACDataLink
{
	public abstract class RS485Device
	{
		/// <summary>
		/// Gets the address of this RS485 Device
		/// </summary>
		public int Address
		{
			get;
			private set;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="address"></param>
		public RS485Device(int address)
		{
			this.Address = address;

			// Listen to events from the RS485 bus
			RS485Bus.Instance.MessageReceived += new RS485Bus.MessageReceivedEventHandler(RS485_MessageReceived);
		}

		/// <summary>
		/// Writes a message to the device over the RS485 Bus
		/// </summary>
		/// <param name="message"></param>
		protected void SendMessage(int address, string message)
		{
			RS485Bus.Instance.SendMessage(address, message);
		}

		/// <summary>
		/// Handle a message sent to this device specifically.
		/// </summary>
		/// <param name="e"></param>
		protected abstract void HandleMessage(RS485Bus.MessageReceivedEventArgs e);

		/// <summary>
		/// Handle message received over the bus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RS485_MessageReceived(object sender, RS485Bus.MessageReceivedEventArgs e)
		{
			// Do something only if the message is intended for us
			if (e.Address == this.Address)
			{
				this.HandleMessage(e);
			}
		}
	}
}
