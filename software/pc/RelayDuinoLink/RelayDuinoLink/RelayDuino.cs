﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHACDataLink
{
	public class RelayDuino : RS485Device
	{

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="address">Address of the relayduino</param>
		public RelayDuino(int address)
			: base(address)
		{

		}

		/// <summary>
		/// Scans for and returns any relayduino devices on the bus.
		/// </summary>
		/// <param name="maxAddress">Highest address number to test</param>
		/// <returns>Array of relayduino objects</returns>
		public static RelayDuino[] ScanForDevices(int maxAddress = 5)
		{
			List<RelayDuino> devices = new List<RelayDuino>();
			RS485Bus.MessageReceivedEventHandler messageHandler = new RS485Bus.MessageReceivedEventHandler(delegate(object sender, RS485Bus.MessageReceivedEventArgs e)
			{
				
			});

			// Listen to messages from the bus
			RS485Bus.Instance.MessageReceived += messageHandler;

			// Go through addresses and send out scan commands
			for (int i = 1; i < maxAddress; i++)
			{
				RS485Bus.Instance.SendMessage(i, "@" + i.ToString("00") + " IS 0\r");
			}

			// Stop listening to messages from the bus
			RS485Bus.Instance.MessageReceived -= messageHandler;

			// All done, return found devices
			return devices.ToArray();
		}

		/// <summary>
		/// Handle messages on the bus sent to this device
		/// </summary>
		/// <param name="e"></param>
		protected override void HandleMessage(RS485Bus.MessageReceivedEventArgs e)
		{
			
		}

	}
}
