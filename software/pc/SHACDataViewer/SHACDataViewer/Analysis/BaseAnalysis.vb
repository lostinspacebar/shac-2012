﻿Imports System.Runtime.Serialization
Imports System.Reflection
Imports System.ComponentModel

''' <summary>
''' Base analysis class that works with chart data and spits out new data points for the 
''' chart
''' </summary>
<DataContractAttribute()>
<KnownTypeAttribute("GetKnownTypes")>
Public MustInherit Class BaseAnalysis

	''' <summary>
	''' Event raised when the analysis item is renamed
	''' </summary>
	Public Event Renamed(ByVal sender As BaseAnalysis, ByVal e As EventArgs)

	''' <summary>
	''' Event raised when the analysis item is renamed
	''' </summary>
	Public Event OptionsChanged(ByVal sender As BaseAnalysis, ByVal e As EventArgs)

	''' <summary>
	''' Map of data that can updated as needed in the Analyze function.
	''' </summary>
	Public AnalyzedData As C5.HashDictionary(Of String, AnalysisDataPoint()) = New C5.HashDictionary(Of String, AnalysisDataPoint())

	''' <summary>
	''' Gets or sets the name for the analysis instance shown in the data chart. This is the name of a particular 
	''' instance of the analysis item
	''' </summary>
	''' <remarks></remarks>
	Private _instanceName As String
	<DataMember()>
	Public Property InstanceName() As String
		Get

			Return _instanceName

		End Get
		Set(ByVal value As String)

			' Save Value
			_instanceName = value

			' Raise event
			RaiseEvent Renamed(Me, EventArgs.Empty)

		End Set
	End Property

	<DataMember()>
	Public ReadOnly ID As String

	''' <summary>
	''' Function that needs to be implemented to updated "analyzed" set of data to be
	''' shown in a data chart
	''' </summary>
	''' <param name="sourceData">Map of source data from the database for this data chart</param>
	Public MustOverride Sub Analyze(ByVal sourceData As C5.HashDictionary(Of Metric, MetricDataPoint()))

	''' <summary>
	''' Constructor
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()

		InstanceName = AnalysisReflectionHelpers.GetAnalysisName(Me.GetType())
		ID = Guid.NewGuid().ToString()

	End Sub

	''' <summary>
	''' Notify of changes to analysis
	''' </summary>
	''' <remarks></remarks>
	Public Sub NotifyUpdate()

		' Raise event
		RaiseEvent OptionsChanged(Me, EventArgs.Empty)

	End Sub

	''' <summary>
	''' Get known types for serialization
	''' </summary>
	Shared Function GetKnownTypes() As IEnumerable(Of Type)

		Return (From a In Assembly.GetExecutingAssembly().GetTypes()
		 Where a.IsSubclassOf(GetType(BaseAnalysis))
		 Select a)

	End Function

	Public Class AnalysisDataPoint

		Public Property X As Object
		Public Property Y As Object

		Public Sub New(ByVal _x As Object, ByVal _y As Object)
			X = _x
			Y = _y
		End Sub

	End Class

End Class
