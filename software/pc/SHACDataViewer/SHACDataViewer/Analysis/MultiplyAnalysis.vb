﻿Imports System.Runtime.Serialization

<DataContractAttribute()>
Public Class MultiplyAnalysis
	Inherits BaseAnalysis

	' Common property for all assemblies.
	Public Shared Property Name As String = "Awesome Multiplication"

	' First metric being used in this subtraction
	<DataMember()>
	Public Property MetricOperand As Metric = Nothing

	<DataMember()>
	Public Property Multiplier As Double = 0.1

	''' <summary>
	''' Analyze function. This does the dirty work. Uses some source data and updates the output data 
	''' (AnalysisData) for this analysis.
	''' </summary>
	''' <param name="sourceData"></param>
	''' <remarks></remarks>
	Public Overrides Sub Analyze(ByVal sourceData As C5.HashDictionary(Of Metric, MetricDataPoint()))

		' Make sure operand is valid
		If MetricOperand Is Nothing Then

			' Nooooooo
			Return

		End If

		' We have both operands, subtract data points

		' Create array with enough spots to hold same amount of data as the operand. 
		Dim multiplied = New List(Of AnalysisDataPoint)
		Dim data = sourceData(MetricOperand)

		' Go through first operand's length and subtract second operand's values from it
		For i As Integer = 0 To data.Length - 1
			multiplied.Add(New AnalysisDataPoint(data(i).Timestamp, data(i).Value * Multiplier))
		Next

		' Save in output array
		AnalyzedData.Add("MultiplicationResults", multiplied.ToArray())

	End Sub


End Class
