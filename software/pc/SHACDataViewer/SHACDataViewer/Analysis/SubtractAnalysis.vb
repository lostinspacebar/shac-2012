﻿Imports System.Runtime.Serialization

<DataContractAttribute()>
Public Class SubtractAnalysis
	Inherits BaseAnalysis

	' Common property for all assemblies.
	Public Shared Property Name As String = "Subtraction"

	' First metric being used in this subtraction
	<DataMember()>
	Public Property MetricOperand1 As Metric = Nothing

	' Second metric being used in this subtraction
	<DataMember()>
	Public Property MetricOperand2 As Metric = Nothing

	''' <summary>
	''' Analyze function. This does the dirty work. Uses some source data and updates the output data 
	''' (AnalysisData) for this analysis.
	''' </summary>
	''' <param name="sourceData"></param>
	''' <remarks></remarks>
	Public Overrides Sub Analyze(ByVal sourceData As C5.HashDictionary(Of Metric, MetricDataPoint()))

		' Make sure both operands are specified
		If MetricOperand1 Is Nothing Or MetricOperand2 Is Nothing Then

			' One or both not specified
			Return

		End If

		' We have both operands, subtract data points

		' Create array with enough spots to hold same amount of data as the first operand. 
		Dim subtracted = New List(Of AnalysisDataPoint)
		Dim data1 = sourceData(MetricOperand1)
		Dim data2 = sourceData(MetricOperand2)

		' Go through first operand's length and subtract second operand's values from it
		For i As Integer = 0 To data1.Length - 1
			' Make sure there is enough data in metricOperand2's side
			If data2.Length > i Then
				subtracted.Add(New AnalysisDataPoint(data1(i).Timestamp, data1(i).Value - data2(i).Value))
			End If
		Next

		' Save in output array
		AnalyzedData.Add("SubtractionResults", subtracted.ToArray())

	End Sub


End Class
