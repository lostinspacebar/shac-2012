﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AnalysisPickerDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.analysisSelect = New System.Windows.Forms.ListBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cancelButton = New System.Windows.Forms.Button()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.okButton = New System.Windows.Forms.Button()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'analysisSelect
		'
		Me.analysisSelect.Dock = System.Windows.Forms.DockStyle.Fill
		Me.analysisSelect.FormattingEnabled = True
		Me.analysisSelect.ItemHeight = 17
		Me.analysisSelect.Location = New System.Drawing.Point(0, 0)
		Me.analysisSelect.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.analysisSelect.Name = "analysisSelect"
		Me.analysisSelect.Size = New System.Drawing.Size(324, 316)
		Me.analysisSelect.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cancelButton)
		Me.Panel1.Controls.Add(Me.Panel2)
		Me.Panel1.Controls.Add(Me.okButton)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 316)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Padding = New System.Windows.Forms.Padding(5)
		Me.Panel1.Size = New System.Drawing.Size(324, 40)
		Me.Panel1.TabIndex = 1
		'
		'cancelButton
		'
		Me.cancelButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.cancelButton.Location = New System.Drawing.Point(152, 5)
		Me.cancelButton.Name = "cancelButton"
		Me.cancelButton.Size = New System.Drawing.Size(81, 30)
		Me.cancelButton.TabIndex = 1
		Me.cancelButton.Text = "Cancel"
		Me.cancelButton.UseVisualStyleBackColor = True
		'
		'Panel2
		'
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
		Me.Panel2.Location = New System.Drawing.Point(233, 5)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(5, 30)
		Me.Panel2.TabIndex = 2
		'
		'okButton
		'
		Me.okButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.okButton.Location = New System.Drawing.Point(238, 5)
		Me.okButton.Name = "okButton"
		Me.okButton.Size = New System.Drawing.Size(81, 30)
		Me.okButton.TabIndex = 0
		Me.okButton.Text = "Add"
		Me.okButton.UseVisualStyleBackColor = True
		'
		'AnalysisPickerDialog
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(324, 356)
		Me.Controls.Add(Me.analysisSelect)
		Me.Controls.Add(Me.Panel1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "AnalysisPickerDialog"
		Me.Text = "Analysis Picker"
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents analysisSelect As System.Windows.Forms.ListBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cancelButton As System.Windows.Forms.Button
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents okButton As System.Windows.Forms.Button
End Class
