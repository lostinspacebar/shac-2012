﻿Imports System.Reflection

Public Class AnalysisPickerDialog

	Private _analysisClasses As List(Of System.Type) = New List(Of System.Type)

	Public SelectedAnalysis As System.Type

	Public Function ShowDialog(ByVal owner As IWin32Window) As DialogResult

		' Get all analysis classes
		Dim classes = From a In Assembly.GetExecutingAssembly().GetTypes()
		 Where a.IsSubclassOf(GetType(BaseAnalysis))
		 Select a

		' Populate list
		For Each a In classes
			analysisSelect.Items.Add(AnalysisReflectionHelpers.GetAnalysisName(a))
			_analysisClasses.Add(a)
		Next

		If classes.Count() > 0 Then
			analysisSelect.SelectedIndex = 0
		End If

		' Base dialog sruff
		Return MyBase.ShowDialog(owner)

	End Function

	Private Sub cancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelButton.Click

		Me.DialogResult = DialogResult.Cancel
		Me.Hide()

	End Sub

	Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click

		Me.SelectedAnalysis = _analysisClasses.Item(analysisSelect.SelectedIndex)
		Me.DialogResult = DialogResult.OK

	End Sub
End Class