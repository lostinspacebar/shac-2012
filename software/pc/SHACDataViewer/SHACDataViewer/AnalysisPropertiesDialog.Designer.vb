﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AnalysisPropertiesDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.analysisProperties = New System.Windows.Forms.PropertyGrid()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cancelButton = New System.Windows.Forms.Button()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.okButton = New System.Windows.Forms.Button()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'analysisProperties
		'
		Me.analysisProperties.Dock = System.Windows.Forms.DockStyle.Fill
		Me.analysisProperties.HelpVisible = False
		Me.analysisProperties.Location = New System.Drawing.Point(0, 0)
		Me.analysisProperties.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.analysisProperties.Name = "analysisProperties"
		Me.analysisProperties.Size = New System.Drawing.Size(457, 328)
		Me.analysisProperties.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cancelButton)
		Me.Panel1.Controls.Add(Me.Panel2)
		Me.Panel1.Controls.Add(Me.okButton)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 328)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Padding = New System.Windows.Forms.Padding(7)
		Me.Panel1.Size = New System.Drawing.Size(457, 45)
		Me.Panel1.TabIndex = 1
		'
		'cancelButton
		'
		Me.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cancelButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.cancelButton.Location = New System.Drawing.Point(271, 7)
		Me.cancelButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.cancelButton.Name = "cancelButton"
		Me.cancelButton.Size = New System.Drawing.Size(87, 31)
		Me.cancelButton.TabIndex = 1
		Me.cancelButton.Text = "Cancel"
		Me.cancelButton.UseVisualStyleBackColor = True
		'
		'Panel2
		'
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
		Me.Panel2.Location = New System.Drawing.Point(358, 7)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(5, 31)
		Me.Panel2.TabIndex = 2
		'
		'okButton
		'
		Me.okButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.okButton.Location = New System.Drawing.Point(363, 7)
		Me.okButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.okButton.Name = "okButton"
		Me.okButton.Size = New System.Drawing.Size(87, 31)
		Me.okButton.TabIndex = 0
		Me.okButton.Text = "OK"
		Me.okButton.UseVisualStyleBackColor = True
		'
		'AnalysisPropertiesDialog
		'
		Me.AcceptButton = Me.okButton
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.cancelButton = Me.cancelButton
		Me.ClientSize = New System.Drawing.Size(457, 373)
		Me.ControlBox = False
		Me.Controls.Add(Me.analysisProperties)
		Me.Controls.Add(Me.Panel1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "AnalysisPropertiesDialog"
		Me.Text = "Analysis Properties"
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents analysisProperties As System.Windows.Forms.PropertyGrid
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cancelButton As System.Windows.Forms.Button
	Friend WithEvents okButton As System.Windows.Forms.Button
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
