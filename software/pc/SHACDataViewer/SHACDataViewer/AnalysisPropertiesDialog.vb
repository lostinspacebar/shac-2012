﻿Public Class AnalysisPropertiesDialog

	Public Sub New(ByVal analysis As BaseAnalysis, ByVal chart As DataChart)

		' This call is required by the designer.
		InitializeComponent()

		' Setup metric property converter thing wtih new list of metrics
		PropertyGridMetricPicker.Metrics = chart.Metrics.ToArray()

		' Setup editor
		analysisProperties.SelectedObject = analysis

	End Sub

	Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click

		Me.DialogResult = DialogResult.OK
		Me.Hide()

	End Sub

	Private Sub cancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelButton.Click

		Me.DialogResult = DialogResult.Cancel
		Me.Hide()

	End Sub
End Class