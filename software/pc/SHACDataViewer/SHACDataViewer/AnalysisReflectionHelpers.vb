﻿Imports System.Reflection

Module AnalysisReflectionHelpers

	''' <summary>
	''' Gets the name for an analysis type. If a public static "Name" property is defined, the value of that is returned. Otherwise,
	''' the classname is returned.
	''' </summary>
	''' <param name="type"></param>
	''' <returns></returns>
	Public Function GetAnalysisName(ByVal type As System.Type) As String

		' Try to get property named "Name"
		Dim nameProperty = type.GetProperty("Name", Reflection.BindingFlags.Public Or Reflection.BindingFlags.Static Or Reflection.BindingFlags.Instance Or Reflection.BindingFlags.FlattenHierarchy)

		' No Name property, just return class name
		If IsNothing(nameProperty) Then
			Return type.Name
		End If

		' User has specified a Name property, return that.
		Return nameProperty.GetValue(Nothing, Nothing)

	End Function

	Public Function CreateInstance(ByVal _t As System.Type) As BaseAnalysis

		Return Activator.CreateInstance(_t)

	End Function

	Public Function CopyProperties(ByVal src As BaseAnalysis, ByVal dest As BaseAnalysis)

		Dim properties As PropertyInfo() = src.GetType().GetProperties(Reflection.BindingFlags.Public Or Reflection.BindingFlags.Instance Or Reflection.BindingFlags.FlattenHierarchy)
		For Each p In properties
			p.SetValue(dest, p.GetValue(src, Nothing), Nothing)
		Next

	End Function

End Module
