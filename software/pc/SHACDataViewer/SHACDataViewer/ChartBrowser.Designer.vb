﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChartBrowser
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.toolbar = New System.Windows.Forms.ToolStrip()
		Me.addChartButton = New System.Windows.Forms.ToolStripButton()
		Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
		Me.chartListbox = New System.Windows.Forms.ListView()
		Me.chartNameListColumn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.chartListContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.deleteChartItemButton = New System.Windows.Forms.ToolStripMenuItem()
		Me.dummyImageList = New System.Windows.Forms.ImageList(Me.components)
		Me.toolbar.SuspendLayout()
		Me.chartListContextMenu.SuspendLayout()
		Me.SuspendLayout()
		'
		'toolbar
		'
		Me.toolbar.AutoSize = False
		Me.toolbar.BackColor = System.Drawing.Color.WhiteSmoke
		Me.toolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
		Me.toolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.addChartButton, Me.ToolStripLabel1})
		Me.toolbar.Location = New System.Drawing.Point(0, 0)
		Me.toolbar.Name = "toolbar"
		Me.toolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
		Me.toolbar.Size = New System.Drawing.Size(215, 25)
		Me.toolbar.TabIndex = 1
		Me.toolbar.Text = "ToolStrip1"
		'
		'addChartButton
		'
		Me.addChartButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.addChartButton.Image = Global.SHACDataViewer.My.Resources.Resources.add
		Me.addChartButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.addChartButton.Name = "addChartButton"
		Me.addChartButton.Size = New System.Drawing.Size(108, 22)
		Me.addChartButton.Text = "Add Data Chart"
		'
		'ToolStripLabel1
		'
		Me.ToolStripLabel1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ToolStripLabel1.ForeColor = System.Drawing.Color.DimGray
		Me.ToolStripLabel1.Name = "ToolStripLabel1"
		Me.ToolStripLabel1.Size = New System.Drawing.Size(55, 22)
		Me.ToolStripLabel1.Text = "Charts"
		'
		'chartListbox
		'
		Me.chartListbox.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.chartListbox.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chartNameListColumn})
		Me.chartListbox.ContextMenuStrip = Me.chartListContextMenu
		Me.chartListbox.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartListbox.FullRowSelect = True
		Me.chartListbox.GridLines = True
		Me.chartListbox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
		Me.chartListbox.HideSelection = False
		Me.chartListbox.Location = New System.Drawing.Point(0, 25)
		Me.chartListbox.MultiSelect = False
		Me.chartListbox.Name = "chartListbox"
		Me.chartListbox.Size = New System.Drawing.Size(215, 469)
		Me.chartListbox.SmallImageList = Me.dummyImageList
		Me.chartListbox.TabIndex = 2
		Me.chartListbox.UseCompatibleStateImageBehavior = False
		Me.chartListbox.View = System.Windows.Forms.View.Details
		'
		'chartNameListColumn
		'
		Me.chartNameListColumn.Width = 233
		'
		'chartListContextMenu
		'
		Me.chartListContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.deleteChartItemButton})
		Me.chartListContextMenu.Name = "chartListContextMenu"
		Me.chartListContextMenu.Size = New System.Drawing.Size(108, 26)
		'
		'deleteChartItemButton
		'
		Me.deleteChartItemButton.Name = "deleteChartItemButton"
		Me.deleteChartItemButton.Size = New System.Drawing.Size(107, 22)
		Me.deleteChartItemButton.Text = "Delete"
		'
		'dummyImageList
		'
		Me.dummyImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
		Me.dummyImageList.ImageSize = New System.Drawing.Size(5, 25)
		Me.dummyImageList.TransparentColor = System.Drawing.Color.Transparent
		'
		'ChartBrowser
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.Controls.Add(Me.chartListbox)
		Me.Controls.Add(Me.toolbar)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Name = "ChartBrowser"
		Me.Size = New System.Drawing.Size(215, 494)
		Me.toolbar.ResumeLayout(False)
		Me.toolbar.PerformLayout()
		Me.chartListContextMenu.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
    Friend WithEvents toolbar As System.Windows.Forms.ToolStrip
    Friend WithEvents addChartButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents chartListbox As System.Windows.Forms.ListView
    Friend WithEvents dummyImageList As System.Windows.Forms.ImageList
    Friend WithEvents chartNameListColumn As System.Windows.Forms.ColumnHeader
    Friend WithEvents chartListContextMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents deleteChartItemButton As System.Windows.Forms.ToolStripMenuItem

End Class
