﻿Imports System.IO

'--------------------------------------------------------------------------
' Shows a list of charts that can be shown/hidden
'--------------------------------------------------------------------------
Public Class ChartBrowser

	''' <summary>
	''' Event raised when a new chart has been selected in the browser
	''' </summary>
	Public Event SelectedChartChanged(ByVal sender As ChartBrowser, ByVal e As EventArgs)

	''' <summary>
	''' Gets the selected chart in the browser
	''' </summary>
	Private _selectedChart As DataChart
	Public ReadOnly Property SelectedChart() As DataChart
		Get
			Return _selectedChart
		End Get
	End Property


	''' <summary>
	''' Constructor
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()

		' This call is required by the designer.
		InitializeComponent()

		' Listen to events from the global chart collection
		AddHandler DataLogger.Charts.ChartAdded, AddressOf Me.OnChartAdded
		AddHandler DataLogger.Charts.ChartRemoved, AddressOf Me.OnChartRemoved

		' Make sure chart list column is 100% width
		chartNameListColumn.Width = -2

	End Sub


	''' <summary>
	''' Add a new chart to the global chart collection
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub addChartButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addChartButton.Click

		DataLogger.Charts.Add(New DataChart())

	End Sub

	''' <summary>
	''' Handle a chart being added to the system
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub OnChartAdded(ByVal sender As Object, ByVal e As DataChartCollection.DataChartCollectionChangedEventArgs)

		' Add to list
		chartListbox.Items.Add(e.Chart.ID, e.Chart.Name, "")

		' Handle renames to the chart
		AddHandler e.Chart.Renamed, AddressOf Me.OnChartRenamed

		' If this is the first item in the list, select it
		If chartListbox.Items.Count = 1 Then
			chartListbox.SelectedIndices.Add(0)

		End If

	End Sub

	''' <summary>
	''' Handler a chart being renamed
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub OnChartRenamed(ByVal sender As DataChart, ByVal e As EventArgs)

		' Rename item in list
		chartListbox.Items.Item(sender.ID).Text = sender.Name

	End Sub

	''' <summary>
	''' Handle a chart being removed from the system
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub OnChartRemoved(ByVal sender As Object, ByVal e As DataChartCollection.DataChartCollectionChangedEventArgs)

		' Remove handlers
		RemoveHandler e.Chart.Renamed, AddressOf Me.OnChartRenamed

		' REmove from listbox
		chartListbox.Items.RemoveByKey(e.Chart.ID)

		' If any items remaining, select first one
		If chartListbox.Items.Count > 0 Then
			chartListbox.SelectedIndices.Clear()
			chartListbox.SelectedIndices.Add(0)
		Else
			' No charts remaining, set to nothing so chart host disables itself
			_selectedChart = Nothing
			RaiseEvent SelectedChartChanged(Me, EventArgs.Empty)
		End If


	End Sub

	''' <summary>
	''' Handle browser being resized
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	Private Sub ChartBrowser_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize

		chartNameListColumn.Width = -2

	End Sub

	''' <summary>
	''' Handle delete buttno for chart item being clicked
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub deleteChartItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deleteChartItemButton.Click

		' Get ID of selected item
		Dim id = chartListbox.SelectedItems.Item(0).Name

		' Make sure user wants to delete it
		If MessageBox.Show("Are you sure you want to delete chart '" & chartListbox.SelectedItems.Item(0).Text & "'?", "Delete?", MessageBoxButtons.YesNo) = DialogResult.Yes Then

			' Yes. They do.
			DataLogger.Charts.Remove(id)

			' Delete file
			File.Delete(DataLogger.Charts.SaveLocation & id & ".xml")

		End If

	End Sub

	''' <summary>
	''' Only show options if an item is selected
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub chartListContextMenu_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles chartListContextMenu.Opening

		If chartListbox.SelectedItems.Count < 1 Then
			e.Cancel = True

		End If

	End Sub

	''' <summary>
	''' Handle selection being changed
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub chartListbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chartListbox.SelectedIndexChanged

		' Make sure we have a valid selected index
		If chartListbox.SelectedItems.Count <> 1 Then
			' Do nothing
			Return
		End If

		' Get id of the chart item selected
		Dim id = chartListbox.SelectedItems.Item(0).Name

		' Set new chart item
		_selectedChart = DataLogger.Charts.Item(id)

		' Raise event so everyone knows about new chart
		RaiseEvent SelectedChartChanged(Me, EventArgs.Empty)

	End Sub

End Class
