﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConnectionDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.hostnameTextbox = New System.Windows.Forms.TextBox()
		Me.usernameTextbox = New System.Windows.Forms.TextBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.passwordTextbox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.databaseNameTextbox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.okButton = New System.Windows.Forms.Button()
		Me.cancelButton = New System.Windows.Forms.Button()
		Me.testButton = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(10, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(70, 17)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Hostname:"
		'
		'hostnameTextbox
		'
		Me.hostnameTextbox.Location = New System.Drawing.Point(13, 31)
		Me.hostnameTextbox.Margin = New System.Windows.Forms.Padding(4)
		Me.hostnameTextbox.Name = "hostnameTextbox"
		Me.hostnameTextbox.Size = New System.Drawing.Size(460, 25)
		Me.hostnameTextbox.TabIndex = 1
		'
		'usernameTextbox
		'
		Me.usernameTextbox.Location = New System.Drawing.Point(13, 86)
		Me.usernameTextbox.Margin = New System.Windows.Forms.Padding(4)
		Me.usernameTextbox.Name = "usernameTextbox"
		Me.usernameTextbox.Size = New System.Drawing.Size(460, 25)
		Me.usernameTextbox.TabIndex = 3
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(10, 65)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(70, 17)
		Me.Label2.TabIndex = 2
		Me.Label2.Text = "Username:"
		'
		'passwordTextbox
		'
		Me.passwordTextbox.Location = New System.Drawing.Point(13, 141)
		Me.passwordTextbox.Margin = New System.Windows.Forms.Padding(4)
		Me.passwordTextbox.Name = "passwordTextbox"
		Me.passwordTextbox.Size = New System.Drawing.Size(460, 25)
		Me.passwordTextbox.TabIndex = 5
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(10, 120)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(67, 17)
		Me.Label3.TabIndex = 4
		Me.Label3.Text = "Password:"
		'
		'databaseNameTextbox
		'
		Me.databaseNameTextbox.Location = New System.Drawing.Point(13, 196)
		Me.databaseNameTextbox.Margin = New System.Windows.Forms.Padding(4)
		Me.databaseNameTextbox.Name = "databaseNameTextbox"
		Me.databaseNameTextbox.Size = New System.Drawing.Size(460, 25)
		Me.databaseNameTextbox.TabIndex = 7
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(10, 175)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(105, 17)
		Me.Label4.TabIndex = 6
		Me.Label4.Text = "Database Name:"
		'
		'okButton
		'
		Me.okButton.Location = New System.Drawing.Point(398, 245)
		Me.okButton.Name = "okButton"
		Me.okButton.Size = New System.Drawing.Size(75, 25)
		Me.okButton.TabIndex = 8
		Me.okButton.Text = "OK"
		Me.okButton.UseVisualStyleBackColor = True
		'
		'cancelButton
		'
		Me.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cancelButton.Location = New System.Drawing.Point(317, 245)
		Me.cancelButton.Name = "cancelButton"
		Me.cancelButton.Size = New System.Drawing.Size(75, 25)
		Me.cancelButton.TabIndex = 9
		Me.cancelButton.Text = "Cancel"
		Me.cancelButton.UseVisualStyleBackColor = True
		'
		'testButton
		'
		Me.testButton.Location = New System.Drawing.Point(13, 245)
		Me.testButton.Name = "testButton"
		Me.testButton.Size = New System.Drawing.Size(75, 25)
		Me.testButton.TabIndex = 10
		Me.testButton.Text = "Test"
		Me.testButton.UseVisualStyleBackColor = True
		'
		'ConnectionDialog
		'
		Me.AcceptButton = Me.okButton
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.cancelButton = Me.cancelButton
		Me.ClientSize = New System.Drawing.Size(485, 282)
		Me.Controls.Add(Me.testButton)
		Me.Controls.Add(Me.cancelButton)
		Me.Controls.Add(Me.okButton)
		Me.Controls.Add(Me.databaseNameTextbox)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.passwordTextbox)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.usernameTextbox)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.hostnameTextbox)
		Me.Controls.Add(Me.Label1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "ConnectionDialog"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "Connection Details"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents hostnameTextbox As System.Windows.Forms.TextBox
	Friend WithEvents usernameTextbox As System.Windows.Forms.TextBox
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents passwordTextbox As System.Windows.Forms.TextBox
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents databaseNameTextbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents okButton As System.Windows.Forms.Button
	Friend Shadows WithEvents cancelButton As System.Windows.Forms.Button
    Friend WithEvents testButton As System.Windows.Forms.Button
End Class
