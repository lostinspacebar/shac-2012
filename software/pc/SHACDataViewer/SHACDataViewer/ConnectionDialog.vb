﻿Imports MySql.Data.MySqlClient

Public Class ConnectionDialog

	'--------------------------------------------------------------------------
	' OK Button clicked
	'--------------------------------------------------------------------------
	Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click

		' Save Settings
		My.Settings.DatabaseHostname = hostnameTextbox.Text
		My.Settings.DatabaseUsername = usernameTextbox.Text
		My.Settings.DatabasePassword = passwordTextbox.Text
		My.Settings.DatabaseName = databaseNameTextbox.Text
		My.Settings.Save()

		' Set Dialog result to OK and get out
		Me.DialogResult = Windows.Forms.DialogResult.OK
		Me.Hide()

	End Sub

	'--------------------------------------------------------------------------
	' Cancel Button clicked
	'--------------------------------------------------------------------------
	Private Sub cancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelButton.Click

		' Set Dialog result to Cancel and get out
		Me.DialogResult = Windows.Forms.DialogResult.Cancel
		Me.Hide()

	End Sub

	'--------------------------------------------------------------------------
	' Test Button clicked
	'--------------------------------------------------------------------------
	Private Sub testButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles testButton.Click

		' Test connect to the database
		Dim testConnection As MySqlConnection = New MySqlConnection
		testConnection.ConnectionString = String.Format("Data Source={0};Database={1};Username={2};Password={3};", hostnameTextbox.Text, databaseNameTextbox.Text, usernameTextbox.Text, passwordTextbox.Text)

		Try
			' Try connecting
			testConnection.Open()

		Catch ex As Exception
			' Failed
			MessageBox.Show("Could not connect to database because: " + vbCrLf + ex.Message + vbCrLf + "Connection String:" + testConnection.ConnectionString, "Connection test failed")
			Return

		End Try

		'Worked
		MessageBox.Show("Test successful, able to connect.", "Success!")

	End Sub

	'--------------------------------------------------------------------------
	' When form is shown, fill in form
	'--------------------------------------------------------------------------
	Private Sub ConnectionDialog_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown

		' Reload settings for updated values
		My.Settings.Reload()

		' Update text boxes from settings
		hostnameTextbox.Text = My.Settings.DatabaseHostname
		usernameTextbox.Text = My.Settings.DatabaseUsername
		passwordTextbox.Text = My.Settings.DatabasePassword
		databaseNameTextbox.Text = My.Settings.DatabaseName

	End Sub
End Class