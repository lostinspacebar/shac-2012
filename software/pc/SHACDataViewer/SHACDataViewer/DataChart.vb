﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Collections.ObjectModel
Imports System.Threading
Imports System.Xml.Serialization
Imports System.IO
Imports System.Runtime.Serialization

''' <summary>
''' Data chart. Data backing to DataChartHost
''' </summary>
''' <remarks></remarks>
<DataContractAttribute()>
Public Class DataChart

	''' <summary>
	''' Event raised when this chart has been renamed
	''' </summary>
	Public Event Renamed(ByVal sender As DataChart, ByVal e As EventArgs)

	''' <summary>
	''' Event raised when the window for the chart was modified
	''' </summary>
	Public Event OptionsChanged(ByVal sender As DataChart, ByVal e As EventArgs)

	''' <summary>
	''' Event raised when updated data is avaialable from the data chart
	''' </summary>
	Public Event DataReady(ByVal sender As DataChart, ByVal e As DataReadyEventArgs)

	''' <summary>
	''' Event raised when chart options for a metric in the chart have been changed
	''' </summary>
	Public Event MetricChartOptionsChanged(ByVal sender As DataChart, ByVal e As MetricChangeEventArgs)

	''' <summary>
	''' Event raised when the datachart is updating/downloading data. Use this to show progress BARS
	''' </summary>
	Public Event Updating(ByVal sender As DataChart, ByVal e As EventArgs)

	''' <summary>
	''' Event raised when an analysis that is part of the datachart has been renamed
	''' </summary>
	Public Event AnalysisRenamed(ByVal sender As DataChart, ByVal e As AnalysisChangeEventArgs)

	''' <summary>
	''' Event raised when an analysis that is part of the datachart has had some options/properties changed
	''' </summary>
	Public Event AnalysisOptionsChanged(ByVal sender As DataChart, ByVal e As AnalysisChangeEventArgs)

	''' <summary>
	''' Event raised when data from an analysis is ready
	''' </summary>
	Public Event AnalysisDataReady(ByVal sender As DataChart, ByVal e As AnalysisDataReadyEventArgs)

	''' <summary>
	''' Event raised when chart options 
	''' </summary>
	Public Event AnalysisDataChartOptionsChanged(ByVal sender As DataChart, ByVal e As AnalysisDataChartOptionsChangedEventArgs)

	''' <summary>
	''' Unique ID for this chart
	''' </summary>
	<DataMember()> _
	Public ReadOnly ID As String

	''' <summary>
	''' Gets or sets the name of the chart
	''' </summary>
	Private chartName As String = "Untitled Chart"
	<DataMember()> _
	Public Property Name() As String

		Get
			Return chartName
		End Get

		Set(ByVal value As String)
			If value <> chartName Then

				' Save value
				chartName = value

				' Being loaded?
				If loadComplete = False Then
					Return
				End If

				' Raise event so everyone knows
				RaiseEvent Renamed(Me, EventArgs.Empty)

				' Save to file
				Save()

			End If
		End Set

	End Property

	''' <summary>
	''' Gets or sets the type of this chart
	''' </summary>
	Private _chartType As ChartType
	<DataMember()> _
	Public Property Type() As ChartType

		Get
			Return _chartType
		End Get

		Set(ByVal value As ChartType)
			If _chartType <> value Then

				' Save Value
				_chartType = value

				' Option changed
				OnOptionChanged()
			End If
		End Set

	End Property


	''' <summary>
	''' Timespan for the live chart. This determines the amount of 
	''' time shown in the live chart starting from the current date/time 
	''' back into history
	''' </summary>
	''' <remarks></remarks>
	Private _liveChartTimespan As TimeSpan = New TimeSpan(0, 5, 0, 0)
	<DataMember()> _
	Public Property LiveChartTimeSpan() As TimeSpan
		Get
			Return _liveChartTimespan
		End Get
		Set(ByVal value As TimeSpan)
			If value <> _liveChartTimespan Then

				' Save new value
				_liveChartTimespan = value

				' Option changed
				OnOptionChanged()

			End If
		End Set
	End Property

	''' <summary>
	''' Update interval in seconds for the live chart
	''' </summary>
	''' <remarks></remarks>
	Private _liveChartUpdateInterval As Integer = 30
	<DataMember()> _
	Public Property LiveChartUpdateInterval() As Integer
		Get
			Return _liveChartUpdateInterval
		End Get
		Set(ByVal value As Integer)
			If value <> _liveChartUpdateInterval Then

				' Save new value
				_liveChartUpdateInterval = value

				' Option changed
				OnOptionChanged()

			End If
		End Set
	End Property

	''' <summary>
	''' Start time for data shown in history chart view
	''' </summary>
	''' <remarks></remarks>

	Private _historyChartStartTime As DateTime
	<DataMember()> _
	Public Property HistoryChartStartTime() As DateTime
		Get
			Return _historyChartStartTime
		End Get
		Set(ByVal value As DateTime)
			If value <> _historyChartStartTime Then

				' Save new value
				_historyChartStartTime = value

				' Option changed
				OnOptionChanged()

			End If
		End Set
	End Property

	''' <summary>
	''' End time for data shown in the history chart view
	''' </summary>
	''' <remarks></remarks>
	Private _historyChartEndTime As DateTime
	<DataMember()> _
	Public Property HistoryChartEndTime() As DateTime
		Get
			Return _historyChartEndTime
		End Get
		Set(ByVal value As DateTime)
			If value <> _historyChartEndTime Then

				' Save new value
				_historyChartEndTime = value

				' Option changed
				OnOptionChanged()

			End If
		End Set
	End Property

	''' <summary>
	''' Gets or sets whether this data chart is enabled and is 
	''' sending out new data according to it's options. Default: False
	''' </summary>
	''' <remarks></remarks>
	Private _enabled As Boolean = False
	Public Property Enabled() As Boolean

		Get
			Return _enabled
		End Get

		Set(ByVal value As Boolean)

			If value <> _enabled Then

				' Save new vlaue
				_enabled = value

				' Make sure we are not being loaded from file
				If loadComplete = False Then
					Return
				End If

				' Save File
				Save()

				' Queue update
				If _enabled Then
					QueueUpdate()
				End If

			End If

		End Set

	End Property


	''' <summary>
	''' Set of metrics being shown in the chart
	''' </summary>
	''' <remarks></remarks>
	<DataMember()> _
	Public Property Metrics As C5.HashSet(Of Metric) = New C5.HashSet(Of Metric)()

	''' <summary>
	''' Unguarded version of metrics options that are actually serialized
	''' </summary>
	<DataMember(Name:="MetricOptions")>
	Private _metricsOptions As Dictionary(Of String, MetricChartOptions) = New Dictionary(Of String, MetricChartOptions)()

	''' <summary>
	''' Gets options for each metric in the 
	''' </summary>
	''' <remarks></remarks>
	Private _metricsOptionsGuarded As ReadOnlyDictionary(Of String, MetricChartOptions)
	Public Property MetricOptions() As ReadOnlyDictionary(Of String, MetricChartOptions)
		Get
			Return _metricsOptionsGuarded
		End Get
		Set(ByVal value As ReadOnlyDictionary(Of String, MetricChartOptions))
			_metricsOptionsGuarded = value
		End Set
	End Property

	<DataMember()> _
	Public Property AnalysisOptions As Dictionary(Of String, Dictionary(Of String, AnalysisChartOptions)) = New Dictionary(Of String, Dictionary(Of String, AnalysisChartOptions))()

	''' <summary>
	''' Set of analysis items being applied to data in the chart
	''' </summary>
	<DataMember()>
	Public Property AnalysisItems As C5.HashSet(Of BaseAnalysis) = New C5.HashSet(Of BaseAnalysis)()

	''' <summary>
	''' When loading from file, this needs to be set to true so 
	''' properties dont keep calling save like a retard
	''' </summary>
	''' <remarks></remarks>
	Protected loadComplete As Boolean = False

	''' <summary>
	''' Update timer
	''' </summary>
	''' <remarks></remarks>
	Private updateTimer As Threading.Timer

	''' <summary>
	''' Cache of data from last update
	''' </summary>
	''' <remarks></remarks>
	Private dataCache As C5.HashDictionary(Of Metric, MetricDataPoint()) = Nothing

	''' <summary>
	''' Constructor
	''' </summary>
	Public Sub New()

		' Generate unique ID
		ID = System.Guid.NewGuid().ToString()

		' Default values for some things
		_historyChartStartTime = DateTime.Now - New TimeSpan(1, 0, 0, 0)
		_historyChartEndTime = DateTime.Now

		' Initialize chart handlers and such
		Initialize()

	End Sub

	''' <summary>
	''' Save data chart to file
	''' </summary>
	''' <remarks></remarks>
	Public Sub Save()

		' All data charts are currently stored to a folder on the desktop
		Dim filePath = DataLogger.Charts.SaveLocation & Me.ID & ".xml"

		' Delete the file if it already exists
		If File.Exists(filePath) Then
			File.Delete(filePath)
		End If

		' Open stream to file
		Dim stream As FileStream = File.OpenWrite(filePath)

		' Serialize chart to file
		Dim serializer As DataContractSerializer = New DataContractSerializer(GetType(DataChart))
		serializer.WriteObject(stream, Me)

		' Close stream
		stream.Close()

	End Sub

	''' <summary>
	''' Find metric with the key specified
	''' </summary>
	Public Function FindMetricByKey(ByVal key As String) As Metric

		For Each m In Metrics
			If m.Key = key Then
				Return m
			End If
		Next

	End Function

	''' <summary>
	''' Find analysis with the specified key
	''' </summary>
	Public Function FindAnalysisByKey(ByVal key As String) As BaseAnalysis

		For Each a In AnalysisItems
			If a.ID = key Then
				Return a
			End If
		Next

	End Function

	''' <summary>
	''' Loads a datachart from file
	''' </summary>
	''' <param name="filePath">Path to .xml file for the data chart</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Shared Function FromFile(ByVal filePath As String) As DataChart

		' Open stream to file
		Dim stream As FileStream = File.OpenRead(filePath)

		' Create deserializer
		Dim deserializer As DataContractSerializer = New DataContractSerializer(GetType(DataChart))

		' Deserialize
		Dim chart As DataChart = deserializer.ReadObject(stream)

		' Close stream so file is unlocked for other uses
		stream.Close()

		' Chart has been loaded
		chart.Initialize()

		' Return loaded chart
		Return chart

	End Function

	Private Sub Initialize()

		' Metric options readonly collection
		_metricsOptionsGuarded = New ReadOnlyDictionary(Of String, MetricChartOptions)(_metricsOptions)

		' Add handlers to know when metrics have been added/removed
		AddHandler Metrics.ItemsAdded, AddressOf Me.OnMetricsAdded
		AddHandler Metrics.ItemsRemoved, AddressOf Me.OnMetricsRemoved
		AddHandler AnalysisItems.ItemsAdded, AddressOf Me.OnAnalysisAdded
		AddHandler AnalysisItems.ItemsRemoved, AddressOf Me.OnAnalysisRemoved

		' Add handlers for any existing metrics and analysis items
		For Each a In AnalysisItems
			AddHandler a.Renamed, AddressOf OnAnalysisRenamed
			AddHandler a.OptionsChanged, AddressOf OnAnalysisOptionsChanged
		Next
		For Each m In _metricsOptions
			AddHandler m.Value.Changed, AddressOf OnMetricChartOptionsChanged
		Next
		For Each a In AnalysisOptions
			For Each o In a.Value
				AddHandler o.Value.Changed, AddressOf OnAnalysisDataChartOptionsChanged
			Next
		Next

		' Chart "loaded"
		loadComplete = True

	End Sub

	''' <summary>
	''' Function to handle update timer ticks
	''' </summary>
	''' <param name="state"></param>
	''' <remarks></remarks>
	Private Sub UpdateTimerThreadFunction(ByVal state As Object)

		' Make sure we are still enabled
		If Me.Enabled = False Or IsNothing(DataLogger.Database) Then
			Return
		End If

		' Updating....
		RaiseEvent Updating(Me, EventArgs.Empty)

		If Me.Type = ChartType.Live Then
			UpdateLiveData()
		Else
			UpdateHistoryData()
		End If

		' Update Analysis Data
		UpdateAnalysisData()

		' Run timer again for next time if we are a live chart
		If Me.Type = ChartType.Live Then
			updateTimer.Change(_liveChartUpdateInterval * 1000, Timeout.Infinite)
		End If

	End Sub

	''' <summary>
	''' Update analysis generated data
	''' </summary>
	''' <remarks></remarks>
	Public Sub UpdateAnalysisData()

		' Apply all analysis items to data cahce
		For Each a In AnalysisItems

			' Clear previous data
			If a.AnalyzedData Is Nothing Then
				a.AnalyzedData = New C5.HashDictionary(Of String, BaseAnalysis.AnalysisDataPoint())
			End If
			a.AnalyzedData.Clear()

			'Analyze
			a.Analyze(dataCache)

			' Go through and update chart options map
			' Remove old keys
			Dim toRemove = From o In AnalysisOptions(a.ID)
			   Where a.AnalyzedData.Contains(o.Key) = False
			   Select o.Key
			For Each remKey In toRemove.ToArray()
				' Remove handler for options thats is going to go away
				RemoveHandler AnalysisOptions(a.ID)(remKey).Changed, AddressOf Me.OnAnalysisDataChartOptionsChanged

				' Remove it
				AnalysisOptions(a.ID).Remove(remKey)
			Next

			' Add ones not in the map
			Dim toAdd = From d In a.AnalyzedData
			   Where AnalysisOptions(a.ID).ContainsKey(d.Key) = False
			   Select d.Key
			For Each addKey In toAdd.ToArray()
				Dim options = New AnalysisChartOptions(a.ID, addKey)
				AddHandler options.Changed, AddressOf Me.OnAnalysisDataChartOptionsChanged
				AnalysisOptions(a.ID).Add(addKey, options)
			Next

			' Raise event
			RaiseEvent AnalysisDataReady(Me, New AnalysisDataReadyEventArgs(a, a.AnalyzedData))

		Next

	End Sub

	''' <summary>
	''' Update live chart data
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateLiveData()

		' Setup command
		Dim data As C5.HashDictionary(Of Metric, MetricDataPoint()) = New C5.HashDictionary(Of Metric, MetricDataPoint())

		' For each metric in the data chart
		For Each m In Metrics
			' Get data
			Dim metricData = DataLogger.Database.GetData(m.System, m.Sensor, DateTime.Now - _liveChartTimespan, DateTime.Now)

			' Add to main data map
			data.Add(m, metricData)
		Next

		' All done. Raise us an event so any listening hosts can have some fun with data
		RaiseEvent DataReady(Me, New DataReadyEventArgs(data))

		' Save data to cache
		dataCache = data

	End Sub

	''' <summary>
	''' Update history chart data
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateHistoryData()

		Dim data As C5.HashDictionary(Of Metric, MetricDataPoint()) = New C5.HashDictionary(Of Metric, MetricDataPoint())

		' For each metric in the data chart
		For Each m In Metrics
			' Get data
			Dim metricData = DataLogger.Database.GetData(m.System, m.Sensor, HistoryChartStartTime, HistoryChartEndTime)

			' Add to main data map
			data.Add(m, metricData)
		Next

		' All done. Raise us an event so any listening hosts can have some fun with data
		RaiseEvent DataReady(Me, New DataReadyEventArgs(data))

		' Save data to cache
		dataCache = data

	End Sub

	''' <summary>
	''' Queue an update
	''' </summary>
	''' <remarks></remarks>
	Private Sub QueueUpdate()

		' Make sure timer exists
		If IsNothing(updateTimer) Then
			updateTimer = New Timer(New TimerCallback(AddressOf Me.UpdateTimerThreadFunction), Nothing, Timeout.Infinite, Timeout.Infinite)
		End If

		' Queue up an update cause new metrics were added. 
		' We add the one second delay so that a bunch of metrics being added doesn't make the a
		' app freak out
		updateTimer.Change(1000, Timeout.Infinite)

	End Sub

	''' <summary>
	''' Handle metrics being added
	''' </summary>
	Private Sub OnMetricsAdded(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of Metric))

		' Adds a container for options for this new metric
		Dim options = New MetricChartOptions(e.Item.Key)
		AddHandler options.Changed, AddressOf OnMetricChartOptionsChanged
		_metricsOptions.Add(e.Item.Key, options)

		' Save File
		Save()

		' Metrics added, queue an update
		QueueUpdate()

	End Sub

	''' <summary>
	''' Handle chart options for a given metric being changed
	''' </summary>
	Private Sub OnMetricChartOptionsChanged(ByVal sender As MetricChartOptions, ByVal e As EventArgs)

		' Raise your own collected event
		RaiseEvent MetricChartOptionsChanged(Me, New MetricChangeEventArgs(FindMetricByKey(sender.MetricKey)))

		' Save filke
		Save()

	End Sub

	''' <summary>
	''' Handle metrics being remoed
	''' </summary>
	Private Sub OnMetricsRemoved(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of Metric))

		' Remove options container for this metric
		RemoveHandler _metricsOptions(e.Item.Key).Changed, AddressOf OnMetricChartOptionsChanged
		_metricsOptions.Remove(e.Item.Key)

		' Save File
		Save()

	End Sub

	''' <summary>
	''' Handle analysis being added
	''' </summary>
	Private Sub OnAnalysisAdded(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of BaseAnalysis))

		' Add handlers for analysis being renamed/options chnanged etc
		AddHandler e.Item.Renamed, AddressOf OnAnalysisRenamed
		AddHandler e.Item.OptionsChanged, AddressOf OnAnalysisOptionsChanged

		' Add a spot in the options map for this analysis
		AnalysisOptions.Add(e.Item.ID, New Dictionary(Of String, AnalysisChartOptions))

		' Save File
		Save()

		' Analysis added, queue an update
		QueueUpdate()

	End Sub

	''' <summary>
	''' Handle analysis being remoed
	''' </summary>
	Private Sub OnAnalysisRemoved(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of BaseAnalysis))

		' Remove handlers for analysis being renamed/options chnanged etc
		RemoveHandler e.Item.Renamed, AddressOf OnAnalysisRenamed
		RemoveHandler e.Item.OptionsChanged, AddressOf OnAnalysisOptionsChanged

		' Remove handlers for any analysis data chart options
		For Each o In AnalysisOptions(e.Item.ID)
			RemoveHandler o.Value.Changed, AddressOf OnAnalysisDataChartOptionsChanged
		Next

		' Remove spot in the options map for this analysis
		AnalysisOptions.Remove(e.Item.ID)

		' Save File
		Save()

	End Sub

	''' <summary>
	''' Handle analysis being renamed
	''' </summary>
	Private Sub OnAnalysisRenamed(ByVal sender As BaseAnalysis, ByVal e As EventArgs)

		' Raise event
		RaiseEvent AnalysisRenamed(Me, New AnalysisChangeEventArgs(sender))

		' Save file
		Save()

	End Sub

	''' <summary>
	''' Handle analysis options being changed
	''' </summary>
	Private Sub OnAnalysisOptionsChanged(ByVal sender As BaseAnalysis, ByVal e As EventArgs)

		' Raise Event
		RaiseEvent AnalysisOptionsChanged(Me, New AnalysisChangeEventArgs(sender))

		' Save file
		Save()

	End Sub

	''' <summary>
	''' Handle chart options being changed for a given analysis's data
	''' </summary>
	Private Sub OnAnalysisDataChartOptionsChanged(ByVal sender As AnalysisChartOptions, ByVal e As EventArgs)

		' Raise collected event
		RaiseEvent AnalysisDataChartOptionsChanged(Me, New AnalysisDataChartOptionsChangedEventArgs(sender))

		' Save file
		Save()

	End Sub

	Private Sub OnOptionChanged()

		' Make sure we are not being loaded right now
		If loadComplete = False Then
			Return
		End If

		' Save File
		Save()

		' Queue update
		QueueUpdate()

		' Raise event
		RaiseEvent OptionsChanged(Me, EventArgs.Empty)

	End Sub

	''' <summary>
	''' Types of charts. Live = updates periodically, History just shows 
	''' data between two timestamps
	''' </summary>
	Public Enum ChartType

		Live
		History

	End Enum

	''' <summary>
	''' Event data for data ready events raised from this chart
	''' </summary>
	''' <remarks></remarks>
	Public Class DataReadyEventArgs
		Inherits EventArgs

		''' <summary>
		''' Updated set of data from the data chart. This is a map from 
		''' a metric to a set of data points for the metric
		''' </summary>
		Public ReadOnly Data As C5.GuardedDictionary(Of Metric, MetricDataPoint())

		''' <summary>
		''' Constructor
		''' </summary>
		''' <param name="_data"></param>
		''' <remarks></remarks>
		Public Sub New(ByVal _data As C5.HashDictionary(Of Metric, MetricDataPoint()))
			Data = New C5.GuardedDictionary(Of Metric, MetricDataPoint())(_data)

		End Sub

	End Class

	''' <summary>
	''' Event data for events that deal with changes to metrics in the chart
	''' </summary>
	''' <remarks></remarks>
	Public Class MetricChangeEventArgs

		''' <summary>
		''' Metric that was changed somehow
		''' </summary>
		''' <remarks></remarks>
		Public ReadOnly Metric As Metric

		''' <summary>
		''' Constructor
		''' </summary>
		''' <param name="_metric"></param>
		''' <remarks></remarks>
		Public Sub New(ByVal _metric As Metric)
			Metric = _metric
		End Sub

	End Class

	Public Class AnalysisDataChartOptionsChangedEventArgs
		Inherits EventArgs

		Public ReadOnly Options As AnalysisChartOptions

		Public Sub New(ByVal _options As AnalysisChartOptions)
			Options = _options
		End Sub

	End Class

	Public Class AnalysisDataReadyEventArgs
		Inherits EventArgs

		''' <summary>
		''' Gets the analysis that generated this data
		''' </summary>
		''' <remarks></remarks>
		Public ReadOnly Analysis As BaseAnalysis

		''' <summary>
		''' Updated set of data from the data chart. This is a map from 
		''' a metric to a set of data points for the metric
		''' </summary>
		Public ReadOnly Data As C5.GuardedDictionary(Of String, BaseAnalysis.AnalysisDataPoint())

		''' <summary>
		''' Constructor
		''' </summary>
		''' <param name="_data"></param>
		''' <remarks></remarks>
		Public Sub New(ByVal _analysis As BaseAnalysis, ByVal _data As C5.HashDictionary(Of String, BaseAnalysis.AnalysisDataPoint()))
			Data = New C5.GuardedDictionary(Of String, BaseAnalysis.AnalysisDataPoint())(_data)
			Analysis = _analysis
		End Sub

	End Class

	''' <summary>
	''' Event data for an analysis item being chnaged
	''' </summary>
	''' <remarks></remarks>
	Public Class AnalysisChangeEventArgs
		Inherits EventArgs

		''' <summary>
		''' Analysis that was changed somehow
		''' </summary>
		''' <remarks></remarks>
		Public ReadOnly Analysis As BaseAnalysis

		''' <summary>
		''' Constructor
		''' </summary>
		''' <param name="_analysis"></param>
		''' <remarks></remarks>
		Public Sub New(ByVal _analysis As BaseAnalysis)
			Analysis = _analysis
		End Sub

	End Class

	''' <summary>
	''' Options for a single metric being shown in the chart. This 
	''' includes visibility, chart type, etc.
	''' </summary>
	<DataContract()> _
	Public Class MetricChartOptions

		''' <summary>
		''' Event raised when options in here have been changed somehow
		''' </summary>
		Public Event Changed(ByVal sender As MetricChartOptions, ByVal e As EventArgs)

		''' <summary>
		''' Gets or sets whether the metric is shown in the chart
		''' </summary>
		Private _visible As Boolean = True
		<DataMember()> _
		Public Property Visible() As Boolean
			Get
				Return _visible
			End Get
			Set(ByVal value As Boolean)

				' Save value
				_visible = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Gets or sets the marker type for the metric
		''' </summary>
		''' <remarks></remarks>
		Private _chartType As SeriesChartType = SeriesChartType.Point
		<DataMember()> _
		Public Property ChartType() As SeriesChartType
			Get
				Return _chartType
			End Get
			Set(ByVal value As SeriesChartType)

				' Save value
				_chartType = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Gets or sets the marker color for the metric
		''' </summary>
		''' <remarks></remarks>
		Private _markerColor As Color
		<DataMember()> _
		Public Property MarkerColor() As Color
			Get
				Return _markerColor
			End Get
			Set(ByVal value As Color)

				' Save value
				_markerColor = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Key to metric that this options object is for
		''' </summary>
		<DataMember()> _
		Public ReadOnly MetricKey As String

		''' <summary>
		''' Constructor
		''' </summary>
		''' <remarks></remarks>
		Public Sub New(ByVal _metricKey As String)

			' Generate random initial color for the metric
			Dim r = New Random(DateTime.Now.Millisecond)
			_markerColor = Color.FromArgb(r.Next(255), r.Next(255), r.Next(255))

			' Save parent metric reference
			MetricKey = _metricKey

		End Sub

	End Class

	''' <summary>
	''' Options for analysis data being shown in the chart. This 
	''' includes visibility, chart type, etc.
	''' </summary>
	<DataContract()> _
	Public Class AnalysisChartOptions

		''' <summary>
		''' Event raised when options in here have been changed somehow
		''' </summary>
		Public Event Changed(ByVal sender As AnalysisChartOptions, ByVal e As EventArgs)

		''' <summary>
		''' Gets or sets whether the metric is shown in the chart
		''' </summary>
		Private _visible As Boolean = True
		<DataMember()> _
		Public Property Visible() As Boolean
			Get
				Return _visible
			End Get
			Set(ByVal value As Boolean)

				' Save value
				_visible = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Gets or sets the marker type for the metric
		''' </summary>
		''' <remarks></remarks>
		Private _chartType As SeriesChartType = SeriesChartType.Point
		<DataMember()> _
		Public Property ChartType() As SeriesChartType
			Get
				Return _chartType
			End Get
			Set(ByVal value As SeriesChartType)

				' Save value
				_chartType = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Gets or sets the marker color for the metric
		''' </summary>
		''' <remarks></remarks>
		Private _markerColor As Color
		<DataMember()> _
		Public Property MarkerColor() As Color
			Get
				Return _markerColor
			End Get
			Set(ByVal value As Color)

				' Save value
				_markerColor = value

				' Raise event
				RaiseEvent Changed(Me, EventArgs.Empty)

			End Set
		End Property

		''' <summary>
		''' Key to metric that this options object is for
		''' </summary>
		<DataMember()> _
		Public ReadOnly AnalysisID As String

		''' <summary>
		''' Name of the data created by user in their analysis
		''' </summary>
		''' <remarks></remarks>
		<DataMember()> _
		Public ReadOnly AnalysisDataName As String

		''' <summary>
		''' Constructor
		''' </summary>
		''' <remarks></remarks>
		Public Sub New(ByVal _analysisID As String, ByVal _dataName As String)

			' Generate random initial color for the metric
			Dim r = New Random(DateTime.Now.Millisecond)
			_markerColor = Color.FromArgb(r.Next(255), r.Next(255), r.Next(255))

			' Save parent metric reference
			AnalysisID = _analysisID
			AnalysisDataName = _dataName

		End Sub

	End Class

End Class