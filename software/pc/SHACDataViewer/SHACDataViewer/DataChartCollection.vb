﻿''' <summary>
''' Collection of data charts
''' </summary>
''' <remarks></remarks>
Public Class DataChartCollection

    ''' <summary>
    ''' Event raised when a chart has been added
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Event ChartAdded(ByVal sender As Object, ByVal e As DataChartCollectionChangedEventArgs)

    ''' <summary>
    ''' Event raised when a chart has been removed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Event ChartRemoved(ByVal sender As Object, ByVal e As DataChartCollectionChangedEventArgs)

    ''' <summary>
    ''' Indexer
    ''' </summary>
    Default Public ReadOnly Property Item(ByVal index As String) As DataChart
        Get
            Return chartList.Item(index)
        End Get
	End Property

	''' <summary>
	''' Save location for all data charts
	''' </summary>
	Public ReadOnly SaveLocation As String = My.Computer.FileSystem.SpecialDirectories.Desktop & "\\SHACDataViewer\\Charts\\"

    ''' <summary>
    ''' List of charts in this collection
    ''' </summary>
    ''' <remarks></remarks>
    Private chartList As Dictionary(Of String, DataChart) = New Dictionary(Of String, DataChart)

    ''' <summary>
    ''' Adds a chart to the chart collection
    ''' </summary>
    ''' <param name="chart"></param>
    ''' <remarks></remarks>
    Public Function Add(ByVal chart As DataChart) As Boolean

        ' Don't add duplicates
        If chartList.ContainsKey(chart.ID) = True Then
            ' Already contains this item
            Return False

        End If

        ' Add to collection
        chartList.Add(chart.ID, chart)

        ' Raise event
        RaiseEvent ChartAdded(Me, New DataChartCollectionChangedEventArgs(chart))

        ' All done
        Return True

    End Function

    ''' <summary>
    ''' Removes a chart from the chart collection
    ''' </summary>
    ''' <param name="chart"></param>
    ''' <remarks></remarks>
    Public Sub Remove(ByVal chart As DataChart)

        ' Add to collection
        chartList.Remove(chart.ID)

        ' Raise event
        RaiseEvent ChartRemoved(Me, New DataChartCollectionChangedEventArgs(chart))

    End Sub

    ''' <summary>
    ''' Removes a chart from the chart collection
    ''' </summary>
    ''' <param name="chartID"></param>
    ''' <remarks></remarks>
    Public Sub Remove(ByVal chartID As String)

        ' Get chart for this id
        Dim chart = chartList.Item(chartID)

        ' Add to collection
        chartList.Remove(chartID)

        ' Raise event
        RaiseEvent ChartRemoved(Me, New DataChartCollectionChangedEventArgs(chart))

    End Sub

    ''' <summary>
    ''' Event data for collection change events
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DataChartCollectionChangedEventArgs

        ''' <summary>
        ''' Chart that has been added, deleted, or modified
        ''' </summary>
        ''' <remarks></remarks>
        Public ReadOnly Chart As DataChart

        ''' <summary>
        ''' Constructor
        ''' </summary>
        ''' <param name="_chart"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal _chart As DataChart)

            Chart = _chart

        End Sub

    End Class

End Class
