﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DataChartHost
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Me.toolbar = New System.Windows.Forms.ToolStrip()
		Me.chartOptionsButton = New System.Windows.Forms.ToolStripButton()
		Me.chartNameLabel = New System.Windows.Forms.ToolStripLabel()
		Me.chartSplit = New System.Windows.Forms.SplitContainer()
		Me.chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.seperatorPanel = New System.Windows.Forms.Panel()
		Me.optionsPanel = New System.Windows.Forms.Panel()
		Me.historyChartOptionsPanel = New System.Windows.Forms.Panel()
		Me.Panel6 = New System.Windows.Forms.Panel()
		Me.historyChartMaxTimeSelect = New System.Windows.Forms.DateTimePicker()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Panel7 = New System.Windows.Forms.Panel()
		Me.Panel8 = New System.Windows.Forms.Panel()
		Me.historyChartMinTimeSelect = New System.Windows.Forms.DateTimePicker()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.liveChartOptionsPanel = New System.Windows.Forms.Panel()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.liveChartUpdateIntervalValue = New System.Windows.Forms.NumericUpDown()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.chartDataSplit = New System.Windows.Forms.SplitContainer()
		Me.dataSeriesList = New System.Windows.Forms.ListView()
		Me.columnSeriesColor = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.columnSeriesName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.columnSeriesMetricValue = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
		Me.metricListContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.ChartTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.metricPointChartTypeSelectItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.metricLineChartTypeSelectItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.metricAreaChartTypeSelectItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.metricMarkerColorSelectMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
		Me.deleteMetricMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.analysisListContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.analysisDataMarkerColorSelectMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisChartTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisDataPointChartTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisDataLineChartTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisDataAreaChartTypeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisOptionsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.analysisContextMenuSep = New System.Windows.Forms.ToolStripSeparator()
		Me.deleteAnalysisMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
		Me.addAnalysisButton = New System.Windows.Forms.ToolStripButton()
		Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
		Me.liveChartTimespanSelect = New SHACDataViewer.TimespanPicker()
		Me.analysisTree = New SHACDataViewer.SeriesTreeView()
		Me.toolbar.SuspendLayout()
		CType(Me.chartSplit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.chartSplit.Panel1.SuspendLayout()
		Me.chartSplit.Panel2.SuspendLayout()
		Me.chartSplit.SuspendLayout()
		CType(Me.chart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.optionsPanel.SuspendLayout()
		Me.historyChartOptionsPanel.SuspendLayout()
		Me.Panel6.SuspendLayout()
		Me.Panel8.SuspendLayout()
		Me.liveChartOptionsPanel.SuspendLayout()
		Me.Panel2.SuspendLayout()
		CType(Me.liveChartUpdateIntervalValue, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.chartDataSplit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.chartDataSplit.Panel1.SuspendLayout()
		Me.chartDataSplit.Panel2.SuspendLayout()
		Me.chartDataSplit.SuspendLayout()
		Me.metricListContextMenu.SuspendLayout()
		Me.analysisListContextMenu.SuspendLayout()
		Me.ToolStrip1.SuspendLayout()
		Me.SuspendLayout()
		'
		'toolbar
		'
		Me.toolbar.AutoSize = False
		Me.toolbar.BackColor = System.Drawing.Color.WhiteSmoke
		Me.toolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
		Me.toolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.chartOptionsButton, Me.chartNameLabel})
		Me.toolbar.Location = New System.Drawing.Point(0, 0)
		Me.toolbar.Name = "toolbar"
		Me.toolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
		Me.toolbar.Size = New System.Drawing.Size(762, 25)
		Me.toolbar.TabIndex = 2
		Me.toolbar.Text = "ToolStrip1"
		'
		'chartOptionsButton
		'
		Me.chartOptionsButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.chartOptionsButton.Image = Global.SHACDataViewer.My.Resources.Resources.cog
		Me.chartOptionsButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.chartOptionsButton.Name = "chartOptionsButton"
		Me.chartOptionsButton.Size = New System.Drawing.Size(101, 22)
		Me.chartOptionsButton.Text = "Chart Options"
		'
		'chartNameLabel
		'
		Me.chartNameLabel.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chartNameLabel.ForeColor = System.Drawing.Color.DimGray
		Me.chartNameLabel.Name = "chartNameLabel"
		Me.chartNameLabel.Size = New System.Drawing.Size(48, 22)
		Me.chartNameLabel.Text = "Chart"
		'
		'chartSplit
		'
		Me.chartSplit.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
		Me.chartSplit.Location = New System.Drawing.Point(0, 25)
		Me.chartSplit.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.chartSplit.Name = "chartSplit"
		'
		'chartSplit.Panel1
		'
		Me.chartSplit.Panel1.Controls.Add(Me.chart)
		Me.chartSplit.Panel1.Controls.Add(Me.seperatorPanel)
		Me.chartSplit.Panel1.Controls.Add(Me.optionsPanel)
		'
		'chartSplit.Panel2
		'
		Me.chartSplit.Panel2.AutoScroll = True
		Me.chartSplit.Panel2.BackColor = System.Drawing.Color.White
		Me.chartSplit.Panel2.Controls.Add(Me.chartDataSplit)
		Me.chartSplit.Size = New System.Drawing.Size(762, 570)
		Me.chartSplit.SplitterDistance = 529
		Me.chartSplit.SplitterWidth = 5
		Me.chartSplit.TabIndex = 4
		'
		'chart
		'
		ChartArea2.AxisX.LabelStyle.Format = "dd/MM/yy HH:mm:ss"
		ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray
		ChartArea2.AxisX.MinorGrid.Enabled = True
		ChartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.WhiteSmoke
		ChartArea2.AxisX.ScrollBar.Size = 20.0R
		ChartArea2.AxisY.IsStartedFromZero = False
		ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray
		ChartArea2.AxisY.MinorGrid.Enabled = True
		ChartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.WhiteSmoke
		ChartArea2.AxisY.ScrollBar.Size = 20.0R
		ChartArea2.BackColor = System.Drawing.Color.Transparent
		ChartArea2.CursorX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Seconds
		ChartArea2.CursorX.IsUserEnabled = True
		ChartArea2.CursorX.IsUserSelectionEnabled = True
		ChartArea2.CursorY.IsUserEnabled = True
		ChartArea2.CursorY.IsUserSelectionEnabled = True
		ChartArea2.Name = "mainChartArea"
		Me.chart.ChartAreas.Add(ChartArea2)
		Me.chart.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chart.Location = New System.Drawing.Point(0, 0)
		Me.chart.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.chart.Name = "chart"
		Series2.ChartArea = "mainChartArea"
		Series2.Name = "Series1"
		Me.chart.Series.Add(Series2)
		Me.chart.Size = New System.Drawing.Size(529, 504)
		Me.chart.TabIndex = 4
		Me.chart.Text = "Chart1"
		'
		'seperatorPanel
		'
		Me.seperatorPanel.BackColor = System.Drawing.SystemColors.Control
		Me.seperatorPanel.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.seperatorPanel.Location = New System.Drawing.Point(0, 504)
		Me.seperatorPanel.Name = "seperatorPanel"
		Me.seperatorPanel.Size = New System.Drawing.Size(529, 1)
		Me.seperatorPanel.TabIndex = 13
		'
		'optionsPanel
		'
		Me.optionsPanel.BackColor = System.Drawing.Color.White
		Me.optionsPanel.Controls.Add(Me.historyChartOptionsPanel)
		Me.optionsPanel.Controls.Add(Me.liveChartOptionsPanel)
		Me.optionsPanel.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.optionsPanel.Location = New System.Drawing.Point(0, 505)
		Me.optionsPanel.Name = "optionsPanel"
		Me.optionsPanel.Padding = New System.Windows.Forms.Padding(7)
		Me.optionsPanel.Size = New System.Drawing.Size(529, 65)
		Me.optionsPanel.TabIndex = 5
		'
		'historyChartOptionsPanel
		'
		Me.historyChartOptionsPanel.Controls.Add(Me.Panel6)
		Me.historyChartOptionsPanel.Controls.Add(Me.Panel7)
		Me.historyChartOptionsPanel.Controls.Add(Me.Panel8)
		Me.historyChartOptionsPanel.Dock = System.Windows.Forms.DockStyle.Left
		Me.historyChartOptionsPanel.Location = New System.Drawing.Point(425, 7)
		Me.historyChartOptionsPanel.Name = "historyChartOptionsPanel"
		Me.historyChartOptionsPanel.Size = New System.Drawing.Size(336, 51)
		Me.historyChartOptionsPanel.TabIndex = 11
		Me.historyChartOptionsPanel.Visible = False
		'
		'Panel6
		'
		Me.Panel6.Controls.Add(Me.historyChartMaxTimeSelect)
		Me.Panel6.Controls.Add(Me.Label4)
		Me.Panel6.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel6.Location = New System.Drawing.Point(167, 0)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Size = New System.Drawing.Size(160, 51)
		Me.Panel6.TabIndex = 11
		'
		'historyChartMaxTimeSelect
		'
		Me.historyChartMaxTimeSelect.CustomFormat = "MM'/'dd'/'yyyy HH':'mm':'ss"
		Me.historyChartMaxTimeSelect.Dock = System.Windows.Forms.DockStyle.Top
		Me.historyChartMaxTimeSelect.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.historyChartMaxTimeSelect.Location = New System.Drawing.Point(0, 13)
		Me.historyChartMaxTimeSelect.Name = "historyChartMaxTimeSelect"
		Me.historyChartMaxTimeSelect.Size = New System.Drawing.Size(160, 25)
		Me.historyChartMaxTimeSelect.TabIndex = 2
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.ForeColor = System.Drawing.Color.DimGray
		Me.Label4.Location = New System.Drawing.Point(0, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(61, 13)
		Me.Label4.TabIndex = 0
		Me.Label4.Text = "Max. Time"
		'
		'Panel7
		'
		Me.Panel7.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel7.Location = New System.Drawing.Point(160, 0)
		Me.Panel7.Name = "Panel7"
		Me.Panel7.Size = New System.Drawing.Size(7, 51)
		Me.Panel7.TabIndex = 12
		'
		'Panel8
		'
		Me.Panel8.Controls.Add(Me.historyChartMinTimeSelect)
		Me.Panel8.Controls.Add(Me.Label5)
		Me.Panel8.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel8.Location = New System.Drawing.Point(0, 0)
		Me.Panel8.Name = "Panel8"
		Me.Panel8.Size = New System.Drawing.Size(160, 51)
		Me.Panel8.TabIndex = 10
		'
		'historyChartMinTimeSelect
		'
		Me.historyChartMinTimeSelect.CustomFormat = "MM'/'dd'/'yyyy HH':'mm':'ss"
		Me.historyChartMinTimeSelect.Dock = System.Windows.Forms.DockStyle.Top
		Me.historyChartMinTimeSelect.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.historyChartMinTimeSelect.Location = New System.Drawing.Point(0, 13)
		Me.historyChartMinTimeSelect.Name = "historyChartMinTimeSelect"
		Me.historyChartMinTimeSelect.Size = New System.Drawing.Size(160, 25)
		Me.historyChartMinTimeSelect.TabIndex = 1
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.ForeColor = System.Drawing.Color.DimGray
		Me.Label5.Location = New System.Drawing.Point(0, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(59, 13)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "Min. Time"
		'
		'liveChartOptionsPanel
		'
		Me.liveChartOptionsPanel.Controls.Add(Me.Panel2)
		Me.liveChartOptionsPanel.Controls.Add(Me.Panel4)
		Me.liveChartOptionsPanel.Controls.Add(Me.Panel1)
		Me.liveChartOptionsPanel.Dock = System.Windows.Forms.DockStyle.Left
		Me.liveChartOptionsPanel.Location = New System.Drawing.Point(7, 7)
		Me.liveChartOptionsPanel.Name = "liveChartOptionsPanel"
		Me.liveChartOptionsPanel.Size = New System.Drawing.Size(418, 51)
		Me.liveChartOptionsPanel.TabIndex = 10
		Me.liveChartOptionsPanel.Visible = False
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.liveChartUpdateIntervalValue)
		Me.Panel2.Controls.Add(Me.Label2)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel2.Location = New System.Drawing.Point(271, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(120, 51)
		Me.Panel2.TabIndex = 11
		'
		'liveChartUpdateIntervalValue
		'
		Me.liveChartUpdateIntervalValue.Dock = System.Windows.Forms.DockStyle.Top
		Me.liveChartUpdateIntervalValue.Location = New System.Drawing.Point(0, 13)
		Me.liveChartUpdateIntervalValue.Name = "liveChartUpdateIntervalValue"
		Me.liveChartUpdateIntervalValue.Size = New System.Drawing.Size(120, 25)
		Me.liveChartUpdateIntervalValue.TabIndex = 1
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.DimGray
		Me.Label2.Location = New System.Drawing.Point(0, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(103, 13)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "Update Interval (s)"
		'
		'Panel4
		'
		Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel4.Location = New System.Drawing.Point(264, 0)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(7, 51)
		Me.Panel4.TabIndex = 12
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.liveChartTimespanSelect)
		Me.Panel1.Controls.Add(Me.Label1)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
		Me.Panel1.Location = New System.Drawing.Point(0, 0)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(264, 51)
		Me.Panel1.TabIndex = 10
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.DimGray
		Me.Label1.Location = New System.Drawing.Point(0, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 13)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Time Span Shown"
		'
		'chartDataSplit
		'
		Me.chartDataSplit.BackColor = System.Drawing.SystemColors.Control
		Me.chartDataSplit.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartDataSplit.Location = New System.Drawing.Point(0, 0)
		Me.chartDataSplit.Name = "chartDataSplit"
		Me.chartDataSplit.Orientation = System.Windows.Forms.Orientation.Horizontal
		'
		'chartDataSplit.Panel1
		'
		Me.chartDataSplit.Panel1.Controls.Add(Me.dataSeriesList)
		Me.chartDataSplit.Panel1.Controls.Add(Me.Label3)
		'
		'chartDataSplit.Panel2
		'
		Me.chartDataSplit.Panel2.Controls.Add(Me.analysisTree)
		Me.chartDataSplit.Panel2.Controls.Add(Me.ToolStrip1)
		Me.chartDataSplit.Size = New System.Drawing.Size(228, 570)
		Me.chartDataSplit.SplitterDistance = 286
		Me.chartDataSplit.SplitterWidth = 5
		Me.chartDataSplit.TabIndex = 0
		'
		'dataSeriesList
		'
		Me.dataSeriesList.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.dataSeriesList.CheckBoxes = True
		Me.dataSeriesList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnSeriesColor, Me.columnSeriesName, Me.columnSeriesMetricValue})
		Me.dataSeriesList.ContextMenuStrip = Me.metricListContextMenu
		Me.dataSeriesList.Dock = System.Windows.Forms.DockStyle.Fill
		Me.dataSeriesList.FullRowSelect = True
		Me.dataSeriesList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
		Me.dataSeriesList.Location = New System.Drawing.Point(0, 25)
		Me.dataSeriesList.MultiSelect = False
		Me.dataSeriesList.Name = "dataSeriesList"
		Me.dataSeriesList.Size = New System.Drawing.Size(228, 261)
		Me.dataSeriesList.Sorting = System.Windows.Forms.SortOrder.Ascending
		Me.dataSeriesList.TabIndex = 13
		Me.dataSeriesList.UseCompatibleStateImageBehavior = False
		Me.dataSeriesList.View = System.Windows.Forms.View.Details
		'
		'columnSeriesColor
		'
		Me.columnSeriesColor.Width = 30
		'
		'columnSeriesName
		'
		Me.columnSeriesName.Width = 132
		'
		'metricListContextMenu
		'
		Me.metricListContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChartTypeToolStripMenuItem, Me.metricMarkerColorSelectMenuItem, Me.ToolStripMenuItem1, Me.deleteMetricMenuItem})
		Me.metricListContextMenu.Name = "metricListContextMenu"
		Me.metricListContextMenu.Size = New System.Drawing.Size(144, 76)
		'
		'ChartTypeToolStripMenuItem
		'
		Me.ChartTypeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.metricPointChartTypeSelectItem, Me.metricLineChartTypeSelectItem, Me.metricAreaChartTypeSelectItem})
		Me.ChartTypeToolStripMenuItem.Name = "ChartTypeToolStripMenuItem"
		Me.ChartTypeToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
		Me.ChartTypeToolStripMenuItem.Text = "Chart Type"
		'
		'metricPointChartTypeSelectItem
		'
		Me.metricPointChartTypeSelectItem.Name = "metricPointChartTypeSelectItem"
		Me.metricPointChartTypeSelectItem.Size = New System.Drawing.Size(102, 22)
		Me.metricPointChartTypeSelectItem.Text = "Point"
		'
		'metricLineChartTypeSelectItem
		'
		Me.metricLineChartTypeSelectItem.Name = "metricLineChartTypeSelectItem"
		Me.metricLineChartTypeSelectItem.Size = New System.Drawing.Size(102, 22)
		Me.metricLineChartTypeSelectItem.Text = "Line"
		'
		'metricAreaChartTypeSelectItem
		'
		Me.metricAreaChartTypeSelectItem.Name = "metricAreaChartTypeSelectItem"
		Me.metricAreaChartTypeSelectItem.Size = New System.Drawing.Size(102, 22)
		Me.metricAreaChartTypeSelectItem.Text = "Area"
		'
		'metricMarkerColorSelectMenuItem
		'
		Me.metricMarkerColorSelectMenuItem.Name = "metricMarkerColorSelectMenuItem"
		Me.metricMarkerColorSelectMenuItem.Size = New System.Drawing.Size(143, 22)
		Me.metricMarkerColorSelectMenuItem.Text = "Marker Color"
		'
		'ToolStripMenuItem1
		'
		Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
		Me.ToolStripMenuItem1.Size = New System.Drawing.Size(140, 6)
		'
		'deleteMetricMenuItem
		'
		Me.deleteMetricMenuItem.Name = "deleteMetricMenuItem"
		Me.deleteMetricMenuItem.Size = New System.Drawing.Size(143, 22)
		Me.deleteMetricMenuItem.Text = "Delete"
		'
		'Label3
		'
		Me.Label3.BackColor = System.Drawing.Color.WhiteSmoke
		Me.Label3.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.DimGray
		Me.Label3.Location = New System.Drawing.Point(0, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(228, 25)
		Me.Label3.TabIndex = 12
		Me.Label3.Text = "Data"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'analysisListContextMenu
		'
		Me.analysisListContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.analysisDataMarkerColorSelectMenuItem, Me.analysisChartTypeMenuItem, Me.analysisOptionsMenuItem, Me.analysisContextMenuSep, Me.deleteAnalysisMenuItem})
		Me.analysisListContextMenu.Name = "analysisListContextMenu"
		Me.analysisListContextMenu.Size = New System.Drawing.Size(153, 120)
		'
		'analysisDataMarkerColorSelectMenuItem
		'
		Me.analysisDataMarkerColorSelectMenuItem.Name = "analysisDataMarkerColorSelectMenuItem"
		Me.analysisDataMarkerColorSelectMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisDataMarkerColorSelectMenuItem.Text = "Marker Color"
		'
		'analysisChartTypeMenuItem
		'
		Me.analysisChartTypeMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.analysisDataPointChartTypeMenuItem, Me.analysisDataLineChartTypeMenuItem, Me.analysisDataAreaChartTypeMenuItem})
		Me.analysisChartTypeMenuItem.Name = "analysisChartTypeMenuItem"
		Me.analysisChartTypeMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisChartTypeMenuItem.Text = "Chart Type"
		'
		'analysisDataPointChartTypeMenuItem
		'
		Me.analysisDataPointChartTypeMenuItem.Name = "analysisDataPointChartTypeMenuItem"
		Me.analysisDataPointChartTypeMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisDataPointChartTypeMenuItem.Text = "Point"
		'
		'analysisDataLineChartTypeMenuItem
		'
		Me.analysisDataLineChartTypeMenuItem.Name = "analysisDataLineChartTypeMenuItem"
		Me.analysisDataLineChartTypeMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisDataLineChartTypeMenuItem.Text = "Line"
		'
		'analysisDataAreaChartTypeMenuItem
		'
		Me.analysisDataAreaChartTypeMenuItem.Name = "analysisDataAreaChartTypeMenuItem"
		Me.analysisDataAreaChartTypeMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisDataAreaChartTypeMenuItem.Text = "Area"
		'
		'analysisOptionsMenuItem
		'
		Me.analysisOptionsMenuItem.Name = "analysisOptionsMenuItem"
		Me.analysisOptionsMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.analysisOptionsMenuItem.Text = "Options..."
		'
		'analysisContextMenuSep
		'
		Me.analysisContextMenuSep.Name = "analysisContextMenuSep"
		Me.analysisContextMenuSep.Size = New System.Drawing.Size(149, 6)
		'
		'deleteAnalysisMenuItem
		'
		Me.deleteAnalysisMenuItem.Name = "deleteAnalysisMenuItem"
		Me.deleteAnalysisMenuItem.Size = New System.Drawing.Size(152, 22)
		Me.deleteAnalysisMenuItem.Text = "Delete"
		'
		'ToolStrip1
		'
		Me.ToolStrip1.AutoSize = False
		Me.ToolStrip1.BackColor = System.Drawing.Color.WhiteSmoke
		Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
		Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.addAnalysisButton, Me.ToolStripLabel1})
		Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
		Me.ToolStrip1.Name = "ToolStrip1"
		Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
		Me.ToolStrip1.Size = New System.Drawing.Size(228, 25)
		Me.ToolStrip1.TabIndex = 16
		Me.ToolStrip1.Text = "Analysis"
		'
		'addAnalysisButton
		'
		Me.addAnalysisButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.addAnalysisButton.Image = Global.SHACDataViewer.My.Resources.Resources.add
		Me.addAnalysisButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.addAnalysisButton.Name = "addAnalysisButton"
		Me.addAnalysisButton.Size = New System.Drawing.Size(76, 22)
		Me.addAnalysisButton.Text = "Add New"
		'
		'ToolStripLabel1
		'
		Me.ToolStripLabel1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ToolStripLabel1.ForeColor = System.Drawing.Color.DimGray
		Me.ToolStripLabel1.Name = "ToolStripLabel1"
		Me.ToolStripLabel1.Size = New System.Drawing.Size(67, 22)
		Me.ToolStripLabel1.Text = "Analysis"
		'
		'liveChartTimespanSelect
		'
		Me.liveChartTimespanSelect.Dock = System.Windows.Forms.DockStyle.Left
		Me.liveChartTimespanSelect.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.liveChartTimespanSelect.Location = New System.Drawing.Point(0, 13)
		Me.liveChartTimespanSelect.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.liveChartTimespanSelect.Name = "liveChartTimespanSelect"
		Me.liveChartTimespanSelect.Size = New System.Drawing.Size(258, 38)
		Me.liveChartTimespanSelect.TabIndex = 1
		Me.liveChartTimespanSelect.Value = System.TimeSpan.Parse("00:00:00")
		'
		'analysisTree
		'
		Me.analysisTree.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.analysisTree.CheckBoxes = True
		Me.analysisTree.ContextMenuStrip = Me.analysisListContextMenu
		Me.analysisTree.Dock = System.Windows.Forms.DockStyle.Fill
		Me.analysisTree.Location = New System.Drawing.Point(0, 25)
		Me.analysisTree.Name = "analysisTree"
		Me.analysisTree.Size = New System.Drawing.Size(228, 254)
		Me.analysisTree.TabIndex = 17
		'
		'DataChartHost
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.Controls.Add(Me.chartSplit)
		Me.Controls.Add(Me.toolbar)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "DataChartHost"
		Me.Size = New System.Drawing.Size(762, 595)
		Me.toolbar.ResumeLayout(False)
		Me.toolbar.PerformLayout()
		Me.chartSplit.Panel1.ResumeLayout(False)
		Me.chartSplit.Panel2.ResumeLayout(False)
		CType(Me.chartSplit, System.ComponentModel.ISupportInitialize).EndInit()
		Me.chartSplit.ResumeLayout(False)
		CType(Me.chart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.optionsPanel.ResumeLayout(False)
		Me.historyChartOptionsPanel.ResumeLayout(False)
		Me.Panel6.ResumeLayout(False)
		Me.Panel6.PerformLayout()
		Me.Panel8.ResumeLayout(False)
		Me.Panel8.PerformLayout()
		Me.liveChartOptionsPanel.ResumeLayout(False)
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.liveChartUpdateIntervalValue, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.chartDataSplit.Panel1.ResumeLayout(False)
		Me.chartDataSplit.Panel2.ResumeLayout(False)
		CType(Me.chartDataSplit, System.ComponentModel.ISupportInitialize).EndInit()
		Me.chartDataSplit.ResumeLayout(False)
		Me.metricListContextMenu.ResumeLayout(False)
		Me.analysisListContextMenu.ResumeLayout(False)
		Me.ToolStrip1.ResumeLayout(False)
		Me.ToolStrip1.PerformLayout()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents toolbar As System.Windows.Forms.ToolStrip
	Friend WithEvents chartOptionsButton As System.Windows.Forms.ToolStripButton
	Friend WithEvents chartNameLabel As System.Windows.Forms.ToolStripLabel
	Friend WithEvents chartSplit As System.Windows.Forms.SplitContainer
	Friend WithEvents chart As System.Windows.Forms.DataVisualization.Charting.Chart
	Friend WithEvents optionsPanel As System.Windows.Forms.Panel
	Friend WithEvents liveChartOptionsPanel As System.Windows.Forms.Panel
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents liveChartUpdateIntervalValue As System.Windows.Forms.NumericUpDown
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Panel4 As System.Windows.Forms.Panel
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents historyChartOptionsPanel As System.Windows.Forms.Panel
	Friend WithEvents Panel6 As System.Windows.Forms.Panel
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents Panel7 As System.Windows.Forms.Panel
	Friend WithEvents Panel8 As System.Windows.Forms.Panel
	Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents historyChartMaxTimeSelect As System.Windows.Forms.DateTimePicker
	Friend WithEvents historyChartMinTimeSelect As System.Windows.Forms.DateTimePicker
	Friend WithEvents seperatorPanel As System.Windows.Forms.Panel
	Friend WithEvents liveChartTimespanSelect As SHACDataViewer.TimespanPicker
	Friend WithEvents chartDataSplit As System.Windows.Forms.SplitContainer
	Friend WithEvents dataSeriesList As System.Windows.Forms.ListView
	Friend WithEvents columnSeriesColor As System.Windows.Forms.ColumnHeader
	Friend WithEvents columnSeriesName As System.Windows.Forms.ColumnHeader
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
	Friend WithEvents addAnalysisButton As System.Windows.Forms.ToolStripButton
	Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
	Friend WithEvents metricListContextMenu As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents ChartTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents metricPointChartTypeSelectItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents metricLineChartTypeSelectItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents metricAreaChartTypeSelectItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents metricMarkerColorSelectMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents deleteMetricMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisListContextMenu As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents analysisOptionsMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisContextMenuSep As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents deleteAnalysisMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents columnSeriesMetricValue As System.Windows.Forms.ColumnHeader
	Friend WithEvents analysisTree As SHACDataViewer.SeriesTreeView
	Friend WithEvents analysisDataMarkerColorSelectMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisChartTypeMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisDataPointChartTypeMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisDataLineChartTypeMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents analysisDataAreaChartTypeMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
