﻿Imports System.Windows.Forms.DataVisualization.Charting

''' <summary>
''' Hosts a data chart and allows the user to modify options and data used 
''' in the data chart
''' </summary>
''' <remarks></remarks>
Public Class DataChartHost

	''' <summary>
	''' Gets or sets the data chart being hosted
	''' </summary>
	''' <remarks></remarks>
	Private _dataChart As DataChart = Nothing
	Public Property DataChart() As DataChart

		Get
			Return _dataChart
		End Get

		Set(ByVal value As DataChart)
			SetDataChart(value)
		End Set

	End Property

	''' <summary>
	''' Constructor
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()

		' This call is required by the designer.
		InitializeComponent()

		' Custom sorter for series lists
		dataSeriesList.ListViewItemSorter = New SeriesListSorter()

	End Sub

	''' <summary>
	''' Switch to a new data chart
	''' </summary>
	''' <param name="value"></param>
	''' <remarks></remarks>
	Private Sub SetDataChart(ByVal value As SHACDataViewer.DataChart)

		' Remove handlers and things from previous chart, if it exists
		If IsNothing(_dataChart) = False Then
			' Disable
			_dataChart.Enabled = False

			' Remove handlers
			RemoveHandler _dataChart.Renamed, AddressOf Me.OnChartRenamed
			RemoveHandler _dataChart.OptionsChanged, AddressOf Me.OnChartOptionsChanged
			RemoveHandler _dataChart.DataReady, AddressOf Me.OnChartDataReady
			RemoveHandler _dataChart.Metrics.ItemsAdded, AddressOf Me.OnChartMetricsAdded
			RemoveHandler _dataChart.Metrics.ItemsRemoved, AddressOf Me.OnChartMetricsRemoved
			RemoveHandler _dataChart.MetricChartOptionsChanged, AddressOf Me.OnChartMetricOptionsChanged
			RemoveHandler _dataChart.AnalysisItems.ItemsAdded, AddressOf Me.OnChartAnalysisAdded
			RemoveHandler _dataChart.AnalysisItems.ItemsRemoved, AddressOf Me.OnChartAnalysisRemoved
			RemoveHandler _dataChart.AnalysisRenamed, AddressOf Me.OnChartAnalysisRenamed
			RemoveHandler _dataChart.AnalysisOptionsChanged, AddressOf Me.OnChartAnalysisOptionsChanged
			RemoveHandler _dataChart.AnalysisDataReady, AddressOf Me.OnChartAnalysisDataReady
			RemoveHandler _dataChart.AnalysisOptionsChanged, AddressOf Me.OnChartAnalysisOptionsChanged
			RemoveHandler _dataChart.AnalysisDataChartOptionsChanged, AddressOf Me.OnChartAnalysisDataChartOptionsChanged

			' Remove old chart objects
			chart.Series.Clear()
			dataSeriesList.Items.Clear()

		End If

		If IsNothing(value) Then

			' No chart. Either app startup or all charts deleted
			Me.Enabled = False

		Else

			' Save chart instance
			_dataChart = value

			' Update label
			chartNameLabel.Text = value.Name

			' Update Options UI
			UpdateOptionsUIForChart()

			' Add legends for any existing metrics and analysis
			dataSeriesList.Items.Clear()
			For Each m In _dataChart.Metrics
				AddSeriesForMetric(m)
			Next
			analysisTree.Nodes.Clear()
			For Each a In _dataChart.AnalysisItems
				AddSeriesForAnalysis(a)
			Next

			' Listen for future name changes
			AddHandler value.Renamed, AddressOf Me.OnChartRenamed
			AddHandler value.OptionsChanged, AddressOf Me.OnChartOptionsChanged
			AddHandler value.DataReady, AddressOf Me.OnChartDataReady
			AddHandler _dataChart.Metrics.ItemsAdded, AddressOf Me.OnChartMetricsAdded
			AddHandler _dataChart.Metrics.ItemsRemoved, AddressOf Me.OnChartMetricsRemoved
			AddHandler _dataChart.MetricChartOptionsChanged, AddressOf Me.OnChartMetricOptionsChanged
			AddHandler _dataChart.AnalysisItems.ItemsAdded, AddressOf Me.OnChartAnalysisAdded
			AddHandler _dataChart.AnalysisItems.ItemsRemoved, AddressOf Me.OnChartAnalysisRemoved
			AddHandler _dataChart.AnalysisRenamed, AddressOf Me.OnChartAnalysisRenamed
			AddHandler _dataChart.AnalysisDataReady, AddressOf Me.OnChartAnalysisDataReady
			AddHandler _dataChart.AnalysisOptionsChanged, AddressOf Me.OnChartAnalysisOptionsChanged
			AddHandler _dataChart.AnalysisDataChartOptionsChanged, AddressOf Me.OnChartAnalysisDataChartOptionsChanged

			' Enable chart
			_dataChart.Enabled = True

			' Have a valid chart, enable host
			Me.Enabled = True

		End If

	End Sub

	Public Sub AddSeriesForMetric(ByVal m As Metric)

		' Try to see a series exists for this already (should never be the case)
		Dim seriesIndex = chart.Series.IndexOf(m.Key)

		' Don't have it in the series list (as it should be)
		If seriesIndex = -1 Then

			' Random generator for colors
			Dim r = New Random(DateTime.Now.Millisecond)

			' Series doesn't exist, add it
			Dim newSeries As Series = New Series(m.Key)
			newSeries.ChartType = _dataChart.MetricOptions.Item(m.Key).ChartType
			newSeries.Color = _dataChart.MetricOptions.Item(m.Key).MarkerColor
			newSeries.XValueType = ChartValueType.DateTime
			newSeries.Enabled = _dataChart.MetricOptions.Item(m.Key).Visible
			chart.Series.Add(newSeries)

			' Add series legend in listbox
			Dim newSeriesLegend = New ListViewItem(New String() {"", m.Key})
			newSeriesLegend.UseItemStyleForSubItems = False
			newSeriesLegend.SubItems.Item(0).BackColor = newSeries.Color
			newSeriesLegend.Checked = _dataChart.MetricOptions.Item(m.Key).Visible
			newSeriesLegend.Name = m.Key
			dataSeriesList.Items.Add(newSeriesLegend)

		End If

	End Sub

	Public Sub AddSeriesForAnalysis(ByVal a As BaseAnalysis)

		Dim newAnalysisLegend = New SeriesTreeNode()
		newAnalysisLegend.Name = a.ID
		newAnalysisLegend.Text = a.InstanceName
		analysisTree.Nodes.Add(newAnalysisLegend)

	End Sub

	''' <summary>
	''' Handle a hosted chart's label being changed
	''' </summary>
	Private Sub OnChartRenamed(ByVal sender As DataChart, ByVal e As EventArgs)

		' Update label
		chartNameLabel.Text = sender.Name

	End Sub

	''' <summary>
	''' Handle a hosted chart's options being changed
	''' </summary>
	Private Sub OnChartOptionsChanged(ByVal sender As SHACDataViewer.DataChart, ByVal e As EventArgs)

		UpdateOptionsUIForChart()

	End Sub

	''' <summary>
	''' New chart data
	''' </summary>
	Private Sub OnChartDataReady(ByVal sender As SHACDataViewer.DataChart, ByVal e As SHACDataViewer.DataChart.DataReadyEventArgs)

		For Each metricData In e.Data

			Dim metric = metricData.Key
			Dim data = metricData.Value

			If Me.InvokeRequired Then

				Me.Invoke(New MethodInvoker(
				Sub()

					' Set series data
					chart.Series.Item(metric.Key).Points.Clear()
					For Each p In data
						chart.Series.Item(metric.Key).Points.AddXY(p.Timestamp, p.Value)
					Next

				End Sub))

			End If

		Next

	End Sub

	''' <summary>
	''' Update options UI for current hosted chart
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateOptionsUIForChart()

		If _dataChart.Type = SHACDataViewer.DataChart.ChartType.Live Then
			liveChartOptionsPanel.Visible = True
			historyChartOptionsPanel.Visible = False
		Else
			liveChartOptionsPanel.Visible = False
			historyChartOptionsPanel.Visible = True
		End If

		liveChartTimespanSelect.Value = _dataChart.LiveChartTimeSpan
		liveChartUpdateIntervalValue.Value = _dataChart.LiveChartUpdateInterval
		historyChartMinTimeSelect.Value = _dataChart.HistoryChartStartTime
		historyChartMaxTimeSelect.Value = _dataChart.HistoryChartEndTime

	End Sub

	''' <summary>
	''' Handle metrics being added
	''' </summary>
	Private Sub OnChartMetricsAdded(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of Metric))

		AddSeriesForMetric(e.Item)

	End Sub

	''' <summary>
	''' Handle metrics being remoed
	''' </summary>
	Private Sub OnChartMetricsRemoved(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of Metric))

		' Try to see a series exists for this already (should never be the case)
		Dim seriesIndex = chart.Series.IndexOf(e.Item.Key)

		' Remove series if it exists
		If seriesIndex <> -1 Then

			chart.Series.RemoveAt(seriesIndex)
			dataSeriesList.Items.RemoveByKey(e.Item.Key)

		End If

	End Sub

	''' <summary>
	''' Handle chart display options for a metric being changed
	''' </summary>
	Private Sub OnChartMetricOptionsChanged(ByVal sender As DataChart, ByVal e As DataChart.MetricChangeEventArgs)

		Dim listItem = dataSeriesList.Items(e.Metric.Key)
		
		' Update series and list item options
		chart.Series(e.Metric.Key).MarkerColor = _dataChart.MetricOptions(e.Metric.Key).MarkerColor
		chart.Series(e.Metric.Key).ChartType = _dataChart.MetricOptions(e.Metric.Key).ChartType
		chart.Series(e.Metric.Key).Enabled = _dataChart.MetricOptions(e.Metric.Key).Visible
		listItem.SubItems(0).BackColor = _dataChart.MetricOptions(e.Metric.Key).MarkerColor

	End Sub

	''' <summary>
	''' Handle analysis being added
	''' </summary>
	Private Sub OnChartAnalysisAdded(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of BaseAnalysis))

		AddSeriesForAnalysis(e.Item)

	End Sub

	''' <summary>
	''' Handle analysis being remoed
	''' </summary>
	Private Sub OnChartAnalysisRemoved(ByVal sender As Object, ByVal e As C5.ItemCountEventArgs(Of BaseAnalysis))

		analysisTree.Nodes.RemoveByKey(e.Item.ID)

	End Sub

	''' <summary>
	''' Handle analysis options being changed
	''' </summary>
	Private Sub OnChartAnalysisOptionsChanged(ByVal sender As DataChart, ByVal e As DataChart.AnalysisChangeEventArgs)

		_dataChart.UpdateAnalysisData()

	End Sub

	''' <summary>
	''' Handle analysis being removed
	''' </summary>
	Private Sub OnChartAnalysisRenamed(ByVal sender As DataChart, ByVal e As DataChart.AnalysisChangeEventArgs)

		analysisTree.Nodes(e.Analysis.ID).Text = e.Analysis.InstanceName

	End Sub

	''' <summary>
	''' Handle analysis options being changed
	''' </summary>
	Private Sub OnChartAnalysisDataReady(ByVal sender As DataChart, ByVal e As DataChart.AnalysisDataReadyEventArgs)

		If Me.InvokeRequired Then

			Me.Invoke(New MethodInvoker(
			Sub()
				OnChartAnalysisDataReady(sender, e)
			End Sub))
			Return

		End If

		' Remove any data in the chart for this analysis
		Dim oldSeries = From s In chart.Series
		 Where s.Name.StartsWith(e.Analysis.ID)
		 Select s
		For Each s In oldSeries.ToArray()
			chart.Series.Remove(s)
		Next

		For Each d In e.Data
			Dim newSeries As Series = New Series(e.Analysis.ID & "." & d.Key)
			For Each p In d.Value
				If p IsNot Nothing Then
					newSeries.Points.AddXY(p.X, p.Y)
				End If
			Next
			newSeries.Enabled = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).Visible
			newSeries.Color = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).MarkerColor
			newSeries.MarkerColor = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).MarkerColor
			newSeries.ChartType = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).ChartType
			chart.Series.Add(newSeries)

			' Add a legend for this data set if it doesn't exist
			Dim seriesLegend As SeriesTreeNode
			If analysisTree.Nodes(e.Analysis.ID).Nodes.ContainsKey(d.Key) = False Then
				seriesLegend = New SeriesTreeNode()
				seriesLegend.Name = d.Key
				analysisTree.Nodes(e.Analysis.ID).Nodes.Add(seriesLegend)
			Else
				seriesLegend = analysisTree.Nodes(e.Analysis.ID).Nodes(d.Key)
			End If
			seriesLegend.Checked = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).Visible
			seriesLegend.BackColor = _dataChart.AnalysisOptions(e.Analysis.ID)(d.Key).MarkerColor
			seriesLegend.Text = d.Key
		Next

		chart.ChartAreas(0).RecalculateAxesScale()
		analysisTree.Update()

	End Sub

	Private Sub OnChartAnalysisDataChartOptionsChanged(ByVal sender As DataChart, ByVal e As DataChart.AnalysisDataChartOptionsChangedEventArgs)

		Dim series = chart.Series(e.Options.AnalysisID & "." & e.Options.AnalysisDataName)
		Dim node = analysisTree.Nodes(e.Options.AnalysisID).Nodes(e.Options.AnalysisDataName)
		node.BackColor = e.Options.MarkerColor
		series.Color = e.Options.MarkerColor
		series.MarkerColor = e.Options.MarkerColor
		series.ChartType = e.Options.ChartType
		series.Enabled = e.Options.Visible

	End Sub

	''' <summary>
	''' Change chart basic options (name, type)
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub chartOptionsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chartOptionsButton.Click

		Dim optionsDialog = New DataChartOptionsDialog()
		If optionsDialog.ShowDialog(_dataChart) = DialogResult.OK Then
			_dataChart.Name = optionsDialog.ChartName
			_dataChart.Type = optionsDialog.ChartType
		End If

	End Sub

	Private Sub liveChartTimespanSelect_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles liveChartTimespanSelect.ValueChanged

		If IsNothing(_dataChart) = False Then
			_dataChart.LiveChartTimeSpan = liveChartTimespanSelect.Value
		End If

	End Sub

	Private Sub liveChartUpdateIntervalValue_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles liveChartUpdateIntervalValue.ValueChanged

		If IsNothing(_dataChart) = False Then
			_dataChart.LiveChartUpdateInterval = liveChartUpdateIntervalValue.Value
		End If

	End Sub

	Private Sub historyChartMinTimeSelect_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles historyChartMinTimeSelect.ValueChanged

		If IsNothing(_dataChart) = False Then
			_dataChart.HistoryChartStartTime = historyChartMinTimeSelect.Value
		End If

	End Sub

	Private Sub historyChartMaxTimeSelect_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles historyChartMaxTimeSelect.ValueChanged

		If IsNothing(_dataChart) = False Then
			_dataChart.HistoryChartEndTime = historyChartMaxTimeSelect.Value
		End If

	End Sub

	Private Sub dataSeriesList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles dataSeriesList.ItemChecked

		' Show/Hide associated series
		Dim seriesKey = e.Item.SubItems.Item(1).Text
		_dataChart.MetricOptions(seriesKey).Visible = e.Item.Checked

		' Reset scale
		chart.ChartAreas.Item(0).RecalculateAxesScale()

	End Sub

	Private Sub dataSeriesList_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dataSeriesList.Resize

		Dim list As ListView = sender
		list.Columns(0).Width = 30
		list.Columns(1).Width = list.Width - 35 - 60
		list.Columns(2).Width = 60

	End Sub

	''' <summary>
	''' Add an analysis data set to the chart
	''' </summary>
	Private Sub addAnalysisButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addAnalysisButton.Click

		' Pick an analysis
		Dim d As AnalysisPickerDialog = New AnalysisPickerDialog()
		If d.ShowDialog(Me) = DialogResult.OK Then

			' Picked. Create instance
			Dim analysis As BaseAnalysis = Activator.CreateInstance(d.SelectedAnalysis)

			' Add to data chart
			_dataChart.AnalysisItems.Add(analysis)

		End If

	End Sub

	Private Sub analysisListContextMenu_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles analysisListContextMenu.Opening

		If analysisTree.SelectedNode Is Nothing Then
			e.Cancel = True
			Return
		End If

		Dim node As SeriesTreeNode = analysisTree.SelectedNode
		If node.Parent Is Nothing Then

			analysisDataMarkerColorSelectMenuItem.Visible = False
			analysisChartTypeMenuItem.Visible = False
			analysisOptionsMenuItem.Visible = True
			analysisContextMenuSep.Visible = True
			deleteAnalysisMenuItem.Visible = True

		Else

			analysisDataMarkerColorSelectMenuItem.Visible = True
			analysisChartTypeMenuItem.Visible = True
			analysisOptionsMenuItem.Visible = False
			analysisContextMenuSep.Visible = False
			deleteAnalysisMenuItem.Visible = False

		End If

	End Sub

	Private Sub deleteMetricMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deleteMetricMenuItem.Click

		If dataSeriesList.SelectedIndices.Count < 1 Then
			Return
		End If

		Dim item As ListViewItem = dataSeriesList.SelectedItems.Item(0)
		Dim metric = _dataChart.FindMetricByKey(item.Name)
		If MessageBox.Show("Delete metric '" & metric.Key & "' from chart?", "Delete", MessageBoxButtons.YesNo) = DialogResult.Yes Then
			_dataChart.Metrics.Remove(metric)
		End If

	End Sub

	Private Sub metricListContextMenu_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles metricListContextMenu.Opening

		If dataSeriesList.SelectedIndices.Count < 1 Then
			e.Cancel = True
		End If

	End Sub

	Private Sub analysisOptionsMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles analysisOptionsMenuItem.Click

		Dim item As SeriesTreeNode = analysisTree.SelectedNode
		Dim analysis = _dataChart.FindAnalysisByKey(item.Name)
		Dim analysisCopy = AnalysisReflectionHelpers.CreateInstance(analysis.GetType())
		AnalysisReflectionHelpers.CopyProperties(analysis, analysisCopy)
		Dim optionsDialog As AnalysisPropertiesDialog = New AnalysisPropertiesDialog(analysisCopy, _dataChart)
		If optionsDialog.ShowDialog() = DialogResult.OK Then
			AnalysisReflectionHelpers.CopyProperties(analysisCopy, analysis)
			analysis.NotifyUpdate()
		End If

	End Sub

	Private Sub metricMarkerColorSelectMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles metricMarkerColorSelectMenuItem.Click

		' Get metric associated with the list item
		Dim item As ListViewItem = dataSeriesList.SelectedItems.Item(0)
		Dim metric = _dataChart.FindMetricByKey(item.Name)

		' SEtup color dialog
		Dim colorSelectDialog As New ColorDialog
		colorSelectDialog.Color = _dataChart.MetricOptions(metric.Key).MarkerColor

		' Show dialog 
		If colorSelectDialog.ShowDialog() = DialogResult.OK Then

			' Save new color
			_dataChart.MetricOptions(metric.Key).MarkerColor = colorSelectDialog.Color

		End If

	End Sub

	''' <summary>
	''' Cursor movage
	''' </summary>
	Private Sub chart_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chart.MouseMove

		'chart.ChartAreas(0).CursorX.Position = chart.ChartAreas(0).AxisX.PixelPositionToValue(e.X)
		'For Each s In chart.Series

		'Next

	End Sub

	Private Sub metricPointChartTypeSelectItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles metricPointChartTypeSelectItem.Click

		Dim item As ListViewItem = dataSeriesList.SelectedItems.Item(0)
		_dataChart.MetricOptions(item.Name).ChartType = SeriesChartType.Point

	End Sub

	Private Sub metricLineChartTypeSelectItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles metricLineChartTypeSelectItem.Click

		Dim item As ListViewItem = dataSeriesList.SelectedItems.Item(0)
		_dataChart.MetricOptions(item.Name).ChartType = SeriesChartType.Line

	End Sub

	Private Sub metricAreaChartTypeSelectItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles metricAreaChartTypeSelectItem.Click

		Dim item As ListViewItem = dataSeriesList.SelectedItems.Item(0)
		_dataChart.MetricOptions(item.Name).ChartType = SeriesChartType.Area

	End Sub

	Public Class SeriesListSorter
		Implements IComparer

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

			Dim xItem As ListViewItem = x
			Dim yItem As ListViewItem = y

			Return xItem.SubItems(1).Text.CompareTo(yItem.SubItems(1).Text)

		End Function

	End Class


	Private Sub deleteAnalysisMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deleteAnalysisMenuItem.Click

		If analysisTree.SelectedNode Is Nothing Then
			Return
		End If

		Dim node = analysisTree.SelectedNode
		Dim analysis = _dataChart.FindAnalysisByKey(node.Name)
		If MessageBox.Show("Delete analysis '" & analysis.InstanceName & "' from chart?", "Delete", MessageBoxButtons.YesNo) = DialogResult.Yes Then
			_dataChart.AnalysisItems.Remove(analysis)
		End If

	End Sub

	Private Sub analysisDataMarkerColorSelectMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles analysisDataMarkerColorSelectMenuItem.Click

		Dim item As SeriesTreeNode = analysisTree.SelectedNode

		' SEtup color dialog
		Dim colorSelectDialog As New ColorDialog
		colorSelectDialog.Color = _dataChart.AnalysisOptions(item.Parent.Name)(item.Name).MarkerColor

		' Show dialog 
		If colorSelectDialog.ShowDialog() = DialogResult.OK Then

			' Save new color
			_dataChart.AnalysisOptions(item.Parent.Name)(item.Name).MarkerColor = colorSelectDialog.Color

		End If

	End Sub

	Private Sub analysisDataPointChartTypeMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles analysisDataPointChartTypeMenuItem.Click

		Dim item As SeriesTreeNode = analysisTree.SelectedNode
		_dataChart.AnalysisOptions(item.Parent.Name)(item.Name).ChartType = SeriesChartType.Point

	End Sub

	Private Sub analysisDataLineChartTypeMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles analysisDataLineChartTypeMenuItem.Click

		Dim item As SeriesTreeNode = analysisTree.SelectedNode
		_dataChart.AnalysisOptions(item.Parent.Name)(item.Name).ChartType = SeriesChartType.Line

	End Sub

	Private Sub analysisDataAreaChartTypeMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles analysisDataAreaChartTypeMenuItem.Click

		Dim item As SeriesTreeNode = analysisTree.SelectedNode
		_dataChart.AnalysisOptions(item.Parent.Name)(item.Name).ChartType = SeriesChartType.Area

	End Sub

	Private Sub analysisTree_BeforeCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles analysisTree.BeforeCheck

		If e.Node.Nodes.Count > 0 Then
			e.Cancel = True
		End If

	End Sub

	Private Sub analysisTree_AfterCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles analysisTree.AfterCheck

		If e.Node.Nodes.Count > 0 Then
			' shoudln't happen but w/e
			Return
		End If

		_dataChart.AnalysisOptions(e.Node.Parent.Name)(e.Node.Name).Visible = e.Node.Checked

	End Sub

	Private Sub analysisTree_NodeMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles analysisTree.NodeMouseClick

		analysisTree.SelectedNode = e.Node

	End Sub
End Class
