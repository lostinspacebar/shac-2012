﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DataChartOptionsDialog
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.chartNameTextbox = New System.Windows.Forms.TextBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.chartTypeSelect = New System.Windows.Forms.ComboBox()
		Me.okButton = New System.Windows.Forms.Button()
		Me.cancelOptionsButton = New System.Windows.Forms.Button()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.DarkGray
		Me.Label1.Location = New System.Drawing.Point(7, 7)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(313, 25)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Name"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
		'
		'chartNameTextbox
		'
		Me.chartNameTextbox.Dock = System.Windows.Forms.DockStyle.Top
		Me.chartNameTextbox.Location = New System.Drawing.Point(7, 32)
		Me.chartNameTextbox.Name = "chartNameTextbox"
		Me.chartNameTextbox.Size = New System.Drawing.Size(313, 25)
		Me.chartNameTextbox.TabIndex = 1
		'
		'Label2
		'
		Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.DarkGray
		Me.Label2.Location = New System.Drawing.Point(7, 57)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(313, 25)
		Me.Label2.TabIndex = 2
		Me.Label2.Text = "Type"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
		'
		'chartTypeSelect
		'
		Me.chartTypeSelect.Dock = System.Windows.Forms.DockStyle.Top
		Me.chartTypeSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.chartTypeSelect.FormattingEnabled = True
		Me.chartTypeSelect.Items.AddRange(New Object() {"Live", "History"})
		Me.chartTypeSelect.Location = New System.Drawing.Point(7, 82)
		Me.chartTypeSelect.Name = "chartTypeSelect"
		Me.chartTypeSelect.Size = New System.Drawing.Size(313, 25)
		Me.chartTypeSelect.TabIndex = 3
		'
		'okButton
		'
		Me.okButton.Location = New System.Drawing.Point(245, 119)
		Me.okButton.Name = "okButton"
		Me.okButton.Size = New System.Drawing.Size(75, 34)
		Me.okButton.TabIndex = 4
		Me.okButton.Text = "OK"
		Me.okButton.UseVisualStyleBackColor = True
		'
		'cancelOptionsButton
		'
		Me.cancelOptionsButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cancelOptionsButton.Location = New System.Drawing.Point(164, 119)
		Me.cancelOptionsButton.Name = "cancelOptionsButton"
		Me.cancelOptionsButton.Size = New System.Drawing.Size(75, 34)
		Me.cancelOptionsButton.TabIndex = 5
		Me.cancelOptionsButton.Text = "Cancel"
		Me.cancelOptionsButton.UseVisualStyleBackColor = True
		'
		'DataChartOptionsDialog
		'
		Me.AcceptButton = Me.okButton
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.CancelButton = Me.cancelOptionsButton
		Me.ClientSize = New System.Drawing.Size(327, 163)
		Me.Controls.Add(Me.cancelOptionsButton)
		Me.Controls.Add(Me.okButton)
		Me.Controls.Add(Me.chartTypeSelect)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.chartNameTextbox)
		Me.Controls.Add(Me.Label1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "DataChartOptionsDialog"
		Me.Padding = New System.Windows.Forms.Padding(7)
		Me.Text = "Chart Options"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents chartNameTextbox As System.Windows.Forms.TextBox
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents chartTypeSelect As System.Windows.Forms.ComboBox
	Friend WithEvents okButton As System.Windows.Forms.Button
	Friend WithEvents cancelOptionsButton As System.Windows.Forms.Button
End Class
