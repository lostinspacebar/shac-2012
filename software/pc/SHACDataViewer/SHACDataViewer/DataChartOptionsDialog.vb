﻿Public Class DataChartOptionsDialog

	''' <summary>
	''' Gets the updated chart name
	''' </summary>
	''' <remarks></remarks>
	Private _chartName As String
	Public ReadOnly Property ChartName() As String
		Get
			Return _chartName
		End Get
	End Property

	''' <summary>
	''' Gets the updated chart type
	''' </summary>
	''' <remarks></remarks>
	Private _chartType As DataChart.ChartType
	Public ReadOnly Property ChartType() As String
		Get
			Return _chartType
		End Get
	End Property


	''' <summary>
	''' Show dialog
	''' </summary>
	''' <param name="chart"></param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Overloads Function ShowDialog(ByVal chart As DataChart) As DialogResult

		chartNameTextbox.Text = chart.Name
		If chart.Type = DataChart.ChartType.Live Then
			chartTypeSelect.SelectedIndex = 0
		Else
			chartTypeSelect.SelectedIndex = 1
		End If

		Return MyBase.ShowDialog()

	End Function

	''' <summary>
	''' Accept chagnes
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click

		_chartName = chartNameTextbox.Text
		If chartTypeSelect.SelectedIndex = 0 Then
			_chartType = DataChart.ChartType.Live
		Else
			_chartType = DataChart.ChartType.History
		End If

		' Dialog result is OK
		DialogResult = Windows.Forms.DialogResult.OK

		' Close window
		Hide()

	End Sub

	''' <summary>
	''' Cancel changes
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub cancelOptionsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelOptionsButton.Click

		' Dialog result is Cancel
		DialogResult = Windows.Forms.DialogResult.Cancel

		' Close window
		Hide()

	End Sub
End Class