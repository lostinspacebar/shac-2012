﻿Public Module DataLogger

    ''' <summary>
    ''' Global database instance
    ''' </summary>
    ''' <remarks></remarks>
    Public Database As Database

    ''' <summary>
    ''' List of charts in the system
    ''' </summary>
    ''' <remarks></remarks>
    Public Charts As DataChartCollection = New DataChartCollection()

End Module
