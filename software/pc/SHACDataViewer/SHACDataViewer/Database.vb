﻿Imports MySql.Data.MySqlClient
Imports System.Windows.Forms.DataVisualization.Charting

Public Class Database

	Dim databaseHostname As String
	Dim databaseUsername As String
	Dim databasePassword As String
	Dim databaseName As String
	Dim databaseConnection As MySqlConnection

	Dim readerLock As Object = New Object()

	'--------------------------------------------------------------------------
	' Constructor
	'--------------------------------------------------------------------------
	Sub New(ByVal hostname As String, ByVal username As String, ByVal password As String, ByVal db As String)

		databaseHostname = hostname
		databaseUsername = username
		databasePassword = password
		databaseName = db

	End Sub

    ''' <summary>
    ''' Connects to the database
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function Connect() As Boolean

		' Create new connection instance
		databaseConnection = New MySqlConnection(String.Format("server={0};database={1};Username={2};Password={3};", databaseHostname, databaseName, databaseUsername, databasePassword))

		Try
			' Try to connect
			databaseConnection.Open()

		Catch ex As Exception
			' Failed
			MessageBox.Show("Could not connect to database because: " + vbCrLf + ex.Message + vbCrLf + "Connection String:" + databaseConnection.ConnectionString, "Connection test failed")
			Return False

		End Try

		' Connection successfully
		Return True

	End Function

    ''' <summary>
    ''' Executes an SQL Query and returns a list of rows. Each row contains a 
    ''' map (Dictionary) between column-name -> value.
    ''' </summary>
    ''' <param name="query">Query to run on the database</param>
    ''' <returns>Returns a table of data</returns>
    ''' <remarks></remarks>
	Public Function ExecuteSelect(ByVal query As String) As List(Of Dictionary(Of String, Object))

		Dim data = New List(Of Dictionary(Of String, Object))

		' Build command
		Dim command = databaseConnection.CreateCommand()
		command.CommandText = query

		' Make sure we get exclusive access to the conn
		SyncLock readerLock

			' Execute command
			Dim reader = command.ExecuteReader()

			' Go through reader and get data into list
			While (reader.Read())

				' Create map for this row of data
				Dim rowData = New Dictionary(Of String, Object)

				' Go through columns in this row and add to map
				For i As Integer = 0 To reader.FieldCount - 1
					rowData.Add(reader.GetName(i), reader.GetValue(i))
				Next

				' Add row of data to main data set
				data.Add(rowData)

			End While

			' Close reader
			reader.Close()

		End SyncLock

		' All done, return rows of data
		Return data

	End Function

    ''' <summary>
    ''' Gets data from a specified sensor in a specified system. 
    ''' Only data points with an id greater than afterId are returned and the result set is 
    ''' limited to 'numPoints' number of points
    ''' </summary>
    ''' <param name="systemName">Name of system</param>
    ''' <param name="sensorName">Name of sensor to get data for</param>
    ''' <param name="afterId">Minimum ID of data points returned</param>
    ''' <param name="numPoints">Number of data points to return after ID specified in afterId. Optional: If not specified, 100 is assumed.</param>
    ''' <returns>Array of DataPoints</returns>
    ''' <remarks></remarks>
    Public Function GetData(ByVal systemName As String, ByVal sensorName As String, ByVal afterId As Integer, Optional ByVal numPoints As Integer = 100) As MetricDataPoint()

        ' Build query
        Dim query = "SELECT id, value FROM datapoints WHERE systemName = '{0}' AND sensorName = '{1}' WHERE id > {2} LIMIT numPoints"
        query = String.Format(query, systemName, sensorName, afterId, numPoints)

        ' Build command
        Dim command = databaseConnection.CreateCommand()
        command.CommandText = query

        ' Run command and get data
        Return GetData(command)

    End Function

    ''' <summary>
    ''' Gets an array of data points for the specified system and sensor. 
    ''' Data points are returned only if they were collected between startTime and endTime.
    ''' The resultset is further limited to numPoints number of points
    ''' </summary>
    ''' <param name="systemName"></param>
    ''' <param name="sensorName"></param>
    ''' <param name="startTime"></param>
    ''' <param name="endTime">Optional. If not specified, the current date/time is assumed.</param>
    ''' <param name="numPoints">Optional. If not specified, 100 is assumed.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetData(ByVal systemName As String, ByVal sensorName As String, ByVal startTime As DateTime, Optional ByVal endTime As DateTime = Nothing, Optional ByVal numPoints As Integer = 100) As MetricDataPoint()

        ' Make sure end time actually has a value
        If endTime = Nothing Then
            ' If no end time is specified, the end time is assumed to be RIGHT NOW
            endTime = DateTime.Now

        End If

        ' Build query
		Dim query = "SELECT id, system, sensor, REPLACE(REPLACE(value, 'OFF', '0.0'), 'ON', '1.0') as value, timestamp FROM datapoints WHERE system = ?systemName AND sensor = ?sensorName AND timestamp >= ?startTime AND timestamp <= ?endTime LIMIT ?numPoints"

        ' Build command
		Dim command = databaseConnection.CreateCommand()
		command.CommandText = query
        command.Parameters.AddWithValue("systemName", systemName)
        command.Parameters.AddWithValue("sensorName", sensorName)
        command.Parameters.AddWithValue("startTime", startTime)
        command.Parameters.AddWithValue("endTime", endTime)
        command.Parameters.AddWithValue("numPoints", numPoints)

        ' Run command and get data
        Return GetData(command)

    End Function

    ''' <summary>
    ''' Returns an array of data points given an SQL command
    ''' </summary>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetData(ByVal command As MySqlCommand) As MetricDataPoint()

		Dim data = New List(Of MetricDataPoint)

		' Get exclusive access to connection
		SyncLock readerLock

			' Run query
			Dim reader = command.ExecuteReader()

			' Go through and build an array of data points (reader.Read() advances one row in the table)
			While reader.Read()

				' Get data from this row
				Dim id = IIf(HasColumn(reader, "id"), reader.Item("id"), 0)
				Dim system = IIf(HasColumn(reader, "system"), reader.Item("system"), String.Empty)
				Dim sensor = IIf(HasColumn(reader, "sensor"), reader.Item("sensor"), String.Empty)
				Dim value = IIf(HasColumn(reader, "value"), reader.Item("value"), 0.0)
				Dim timestamp = IIf(HasColumn(reader, "timestamp"), reader.Item("timestamp"), DateTime.MinValue)
				Dim dataPoint = New MetricDataPoint(id, system, sensor, value, timestamp)

				' Add to list of data points
				data.Add(dataPoint)

			End While

			' Close reader
			reader.Close()

		End SyncLock

		' All done, return array of data points
		Return data.ToArray()

	End Function

    ''' <summary>
    ''' Disconnects from the database
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Disconnect() As Boolean

        Try
            databaseConnection.Close()
        Catch ex As Exception
            ' Failed
            MessageBox.Show("Could not disconnect from database because: " + vbCrLf + ex.Message)
            Return False
        End Try

        ' Disconnection successful
        Return True

	End Function

	''' <summary>
	''' Check to see if a column exists in a result set
	''' </summary>
	Private Function HasColumn(ByVal reader As MySqlDataReader, ByVal columnName As String) As Boolean

		For i As Integer = 0 To reader.FieldCount - 1
			If reader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase) Then
				Return True
			End If
		Next

		Return False

	End Function

End Class
