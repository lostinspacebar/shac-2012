﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.toolbar = New System.Windows.Forms.ToolStrip()
		Me.connectionSettingsButton = New System.Windows.Forms.ToolStripButton()
		Me.connectButton = New System.Windows.Forms.ToolStripButton()
		Me.disconnectButton = New System.Windows.Forms.ToolStripButton()
		Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
		Me.statusBar = New System.Windows.Forms.StatusStrip()
		Me.statusLabel = New System.Windows.Forms.ToolStripStatusLabel()
		Me.statusProgressBar = New System.Windows.Forms.ToolStripProgressBar()
		Me.mainSplit = New System.Windows.Forms.SplitContainer()
		Me.chartSplit = New System.Windows.Forms.SplitContainer()
		Me.secretToolbar = New System.Windows.Forms.MenuStrip()
		Me.fullscreenMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.metricBrowser = New SHACDataViewer.MetricBrowser()
		Me.chartBrowser = New SHACDataViewer.ChartBrowser()
		Me.chartHost = New SHACDataViewer.DataChartHost()
		Me.toolbar.SuspendLayout()
		Me.statusBar.SuspendLayout()
		CType(Me.mainSplit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.mainSplit.Panel1.SuspendLayout()
		Me.mainSplit.Panel2.SuspendLayout()
		Me.mainSplit.SuspendLayout()
		CType(Me.chartSplit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.chartSplit.Panel1.SuspendLayout()
		Me.chartSplit.Panel2.SuspendLayout()
		Me.chartSplit.SuspendLayout()
		Me.secretToolbar.SuspendLayout()
		Me.SuspendLayout()
		'
		'toolbar
		'
		Me.toolbar.AutoSize = False
		Me.toolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
		Me.toolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.connectionSettingsButton, Me.connectButton, Me.disconnectButton, Me.ToolStripSeparator1})
		Me.toolbar.Location = New System.Drawing.Point(0, 0)
		Me.toolbar.Name = "toolbar"
		Me.toolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
		Me.toolbar.Size = New System.Drawing.Size(1053, 35)
		Me.toolbar.TabIndex = 0
		Me.toolbar.Text = "ToolStrip1"
		'
		'connectionSettingsButton
		'
		Me.connectionSettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
		Me.connectionSettingsButton.Image = Global.SHACDataViewer.My.Resources.Resources.cog
		Me.connectionSettingsButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.connectionSettingsButton.Margin = New System.Windows.Forms.Padding(5, 5, 0, 5)
		Me.connectionSettingsButton.Name = "connectionSettingsButton"
		Me.connectionSettingsButton.Size = New System.Drawing.Size(23, 25)
		Me.connectionSettingsButton.Text = "Connect"
		'
		'connectButton
		'
		Me.connectButton.Image = Global.SHACDataViewer.My.Resources.Resources.connect
		Me.connectButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.connectButton.Margin = New System.Windows.Forms.Padding(5, 5, 0, 5)
		Me.connectButton.Name = "connectButton"
		Me.connectButton.Size = New System.Drawing.Size(72, 25)
		Me.connectButton.Text = "Connect"
		'
		'disconnectButton
		'
		Me.disconnectButton.Enabled = False
		Me.disconnectButton.Image = Global.SHACDataViewer.My.Resources.Resources.disconnect
		Me.disconnectButton.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.disconnectButton.Margin = New System.Windows.Forms.Padding(5, 5, 0, 5)
		Me.disconnectButton.Name = "disconnectButton"
		Me.disconnectButton.Size = New System.Drawing.Size(86, 25)
		Me.disconnectButton.Text = "Disconnect"
		'
		'ToolStripSeparator1
		'
		Me.ToolStripSeparator1.AutoSize = False
		Me.ToolStripSeparator1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
		Me.ToolStripSeparator1.Padding = New System.Windows.Forms.Padding(5, 5, 0, 5)
		Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
		'
		'statusBar
		'
		Me.statusBar.AutoSize = False
		Me.statusBar.BackColor = System.Drawing.SystemColors.Control
		Me.statusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusLabel, Me.statusProgressBar})
		Me.statusBar.Location = New System.Drawing.Point(0, 552)
		Me.statusBar.Name = "statusBar"
		Me.statusBar.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
		Me.statusBar.Size = New System.Drawing.Size(1053, 30)
		Me.statusBar.SizingGrip = False
		Me.statusBar.TabIndex = 1
		Me.statusBar.Text = "statusBar"
		'
		'statusLabel
		'
		Me.statusLabel.Name = "statusLabel"
		Me.statusLabel.Size = New System.Drawing.Size(82, 25)
		Me.statusLabel.Text = "Disconnected."
		'
		'statusProgressBar
		'
		Me.statusProgressBar.MarqueeAnimationSpeed = 20
		Me.statusProgressBar.Name = "statusProgressBar"
		Me.statusProgressBar.Size = New System.Drawing.Size(150, 24)
		Me.statusProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee
		Me.statusProgressBar.Visible = False
		'
		'mainSplit
		'
		Me.mainSplit.Dock = System.Windows.Forms.DockStyle.Fill
		Me.mainSplit.Enabled = False
		Me.mainSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
		Me.mainSplit.Location = New System.Drawing.Point(0, 35)
		Me.mainSplit.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.mainSplit.Name = "mainSplit"
		'
		'mainSplit.Panel1
		'
		Me.mainSplit.Panel1.BackColor = System.Drawing.Color.White
		Me.mainSplit.Panel1.Controls.Add(Me.metricBrowser)
		'
		'mainSplit.Panel2
		'
		Me.mainSplit.Panel2.BackColor = System.Drawing.Color.White
		Me.mainSplit.Panel2.Controls.Add(Me.chartSplit)
		Me.mainSplit.Size = New System.Drawing.Size(1053, 517)
		Me.mainSplit.SplitterDistance = 236
		Me.mainSplit.SplitterWidth = 5
		Me.mainSplit.TabIndex = 2
		'
		'chartSplit
		'
		Me.chartSplit.BackColor = System.Drawing.SystemColors.Control
		Me.chartSplit.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
		Me.chartSplit.Location = New System.Drawing.Point(0, 0)
		Me.chartSplit.Name = "chartSplit"
		'
		'chartSplit.Panel1
		'
		Me.chartSplit.Panel1.BackColor = System.Drawing.Color.White
		Me.chartSplit.Panel1.Controls.Add(Me.chartBrowser)
		'
		'chartSplit.Panel2
		'
		Me.chartSplit.Panel2.Controls.Add(Me.chartHost)
		Me.chartSplit.Size = New System.Drawing.Size(812, 517)
		Me.chartSplit.SplitterDistance = 188
		Me.chartSplit.SplitterWidth = 5
		Me.chartSplit.TabIndex = 0
		'
		'secretToolbar
		'
		Me.secretToolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fullscreenMenuItem})
		Me.secretToolbar.Location = New System.Drawing.Point(0, 0)
		Me.secretToolbar.Name = "secretToolbar"
		Me.secretToolbar.Size = New System.Drawing.Size(1053, 24)
		Me.secretToolbar.TabIndex = 3
		Me.secretToolbar.Text = "MenuStrip1"
		Me.secretToolbar.Visible = False
		'
		'fullscreenMenuItem
		'
		Me.fullscreenMenuItem.CheckOnClick = True
		Me.fullscreenMenuItem.Name = "fullscreenMenuItem"
		Me.fullscreenMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11
		Me.fullscreenMenuItem.Size = New System.Drawing.Size(76, 20)
		Me.fullscreenMenuItem.Text = "Full Screen"
		'
		'metricBrowser
		'
		Me.metricBrowser.BackColor = System.Drawing.Color.White
		Me.metricBrowser.Dock = System.Windows.Forms.DockStyle.Fill
		Me.metricBrowser.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.metricBrowser.Location = New System.Drawing.Point(0, 0)
		Me.metricBrowser.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.metricBrowser.Name = "metricBrowser"
		Me.metricBrowser.Size = New System.Drawing.Size(236, 517)
		Me.metricBrowser.TabIndex = 0
		'
		'chartBrowser
		'
		Me.chartBrowser.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartBrowser.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chartBrowser.Location = New System.Drawing.Point(0, 0)
		Me.chartBrowser.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.chartBrowser.Name = "chartBrowser"
		Me.chartBrowser.Size = New System.Drawing.Size(188, 517)
		Me.chartBrowser.TabIndex = 0
		'
		'chartHost
		'
		Me.chartHost.DataChart = Nothing
		Me.chartHost.Dock = System.Windows.Forms.DockStyle.Fill
		Me.chartHost.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chartHost.Location = New System.Drawing.Point(0, 0)
		Me.chartHost.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.chartHost.Name = "chartHost"
		Me.chartHost.Size = New System.Drawing.Size(619, 517)
		Me.chartHost.TabIndex = 0
		'
		'MainWindow
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1053, 582)
		Me.Controls.Add(Me.mainSplit)
		Me.Controls.Add(Me.statusBar)
		Me.Controls.Add(Me.toolbar)
		Me.Controls.Add(Me.secretToolbar)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.MainMenuStrip = Me.secretToolbar
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "MainWindow"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "SHAC Data Viewer"
		Me.toolbar.ResumeLayout(False)
		Me.toolbar.PerformLayout()
		Me.statusBar.ResumeLayout(False)
		Me.statusBar.PerformLayout()
		Me.mainSplit.Panel1.ResumeLayout(False)
		Me.mainSplit.Panel2.ResumeLayout(False)
		CType(Me.mainSplit, System.ComponentModel.ISupportInitialize).EndInit()
		Me.mainSplit.ResumeLayout(False)
		Me.chartSplit.Panel1.ResumeLayout(False)
		Me.chartSplit.Panel2.ResumeLayout(False)
		CType(Me.chartSplit, System.ComponentModel.ISupportInitialize).EndInit()
		Me.chartSplit.ResumeLayout(False)
		Me.secretToolbar.ResumeLayout(False)
		Me.secretToolbar.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents toolbar As System.Windows.Forms.ToolStrip
	Friend WithEvents connectButton As System.Windows.Forms.ToolStripButton
	Friend WithEvents disconnectButton As System.Windows.Forms.ToolStripButton
	Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents statusBar As System.Windows.Forms.StatusStrip
	Friend WithEvents statusProgressBar As System.Windows.Forms.ToolStripProgressBar
	Friend WithEvents statusLabel As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents mainSplit As System.Windows.Forms.SplitContainer
	Friend WithEvents chartSplit As System.Windows.Forms.SplitContainer
	Friend WithEvents connectionSettingsButton As System.Windows.Forms.ToolStripButton
	Friend WithEvents chartBrowser As SHACDataViewer.ChartBrowser
	Friend WithEvents metricBrowser As SHACDataViewer.MetricBrowser
	Friend WithEvents chartHost As SHACDataViewer.DataChartHost
	Friend WithEvents secretToolbar As System.Windows.Forms.MenuStrip
	Friend WithEvents fullscreenMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
