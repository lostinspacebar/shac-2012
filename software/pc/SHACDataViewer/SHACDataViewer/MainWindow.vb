﻿Imports System.Threading.Tasks
Imports System.Threading
Imports System.IO

Public Class MainWindow

	''' <summary>
	''' Constructor
	''' </summary>
	''' <remarks></remarks>
	Sub New()

		' This call is required by the designer.
		InitializeComponent()

		' Add event handlers for the chart browser so we can properly
		' select charts
		AddHandler chartBrowser.SelectedChartChanged, AddressOf Me.OnChartBrowserSelectionChanged

		' Add event handlers for the metric browser so we can properly 
		' add metrics to data charts
		AddHandler metricBrowser.MetricAddRequest, AddressOf Me.OnMetricBrowserAddRequest

		' Load data charts from file
		LoadAllDataCharts()

	End Sub

	''' <summary>
	''' Connect to the database
	''' </summary>
	''' <remarks></remarks>
	Sub Connect()

		' Update global instance
		DataLogger.Database = New Database(My.Settings.DatabaseHostname, My.Settings.DatabaseUsername, My.Settings.DatabasePassword, My.Settings.DatabaseName)

		' Connect to database
		If DataLogger.Database.Connect() = True Then

			' Connection succesful
			connectButton.Enabled = False
			disconnectButton.Enabled = True
			connectionSettingsButton.Enabled = False
			statusLabel.Text = "Connected."
			mainSplit.Enabled = True

			' TODO: Hackety Hack Hack. Figure out a better way to do this
			If IsNothing(chartHost.DataChart) = False Then
				chartHost.DataChart.Enabled = False
				chartHost.DataChart.Enabled = True
			End If

		End If

	End Sub

	''' <summary>
	''' Let user change connection options
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function UpdateConnectionSettings() As Boolean

		Dim connectionDialog As ConnectionDialog = New ConnectionDialog()
		Dim buttonPressed As DialogResult = connectionDialog.ShowDialog(Me)

		' Show connection dialog to update info if needed
		If buttonPressed = Windows.Forms.DialogResult.OK Then

			' User changed settings
			Return True

		End If

		' User cancelled
		Return False

	End Function

	''' <summary>
	''' Disconnect from the database
	''' </summary>
	''' <remarks></remarks>
	Sub Disconnect()

		' Disconnect from database
		mainSplit.Enabled = False
		DataLogger.Database.Disconnect()
		DataLogger.Database = Nothing
		connectButton.Enabled = True
		connectionSettingsButton.Enabled = True
		disconnectButton.Enabled = False
		statusLabel.Text = "Disconnected."

	End Sub

	''' <summary>
	''' Handle connect button being clicked
	''' </summary>
	Private Sub connectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles connectButton.Click

		Connect()

	End Sub

	''' <summary>
	''' Handle disconnect button being clicked
	''' </summary>
	Private Sub disconnectButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles disconnectButton.Click

		Disconnect()

	End Sub

	''' <summary>
	''' Handle connection settings button being clicked
	''' </summary>
	Private Sub connectionSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles connectionSettingsButton.Click

		UpdateConnectionSettings()

	End Sub

	''' <summary>
	''' Handle new chart being selected
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub OnChartBrowserSelectionChanged(ByVal sender As SHACDataViewer.ChartBrowser, ByVal e As EventArgs)

		' Remove handlers if there was already a chart
		If IsNothing(chartHost.DataChart) = False Then
			RemoveHandler chartHost.DataChart.Updating, AddressOf Me.OnChartUpdating
			RemoveHandler chartHost.DataChart.DataReady, AddressOf Me.OnChartDataReady
		End If

		' Set new chart
		chartHost.DataChart = sender.SelectedChart

		' Add handlers for new chart
		AddHandler chartHost.DataChart.Updating, AddressOf Me.OnChartUpdating
		AddHandler chartHost.DataChart.DataReady, AddressOf Me.OnChartDataReady

	End Sub

	''' <summary>
	''' Passes selected metrics in teh metric browser to the current datachart in the data chart host
	''' </summary>
	Private Sub OnMetricBrowserAddRequest(ByVal sender As Object, ByVal e As SHACDataViewer.MetricBrowser.MetricSelectionEventArgs)

		If IsNothing(chartHost.DataChart) = False Then
			chartHost.DataChart.Metrics.AddAll(e.Metrics)
		End If

	End Sub

	''' <summary>
	''' Go full screen
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub fullscreenMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fullscreenMenuItem.Click

		If fullscreenMenuItem.Checked Then
			' Make window fill screen
			FormBorderStyle = Windows.Forms.FormBorderStyle.None
			WindowState = FormWindowState.Maximized

			' Hide everything except for chart
			toolbar.Visible = False
			mainSplit.Panel1Collapsed = True
			mainSplit.Panel1.Hide()
			chartSplit.Panel1Collapsed = True
			chartSplit.Panel1.Hide()

		Else
			' Bring window back to normal
			WindowState = FormWindowState.Normal
			FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable

			' show everything again
			toolbar.Visible = True
			mainSplit.Panel1Collapsed = False
			mainSplit.Panel1.Show()
			chartSplit.Panel1Collapsed = False
			chartSplit.Panel1.Show()
		End If

	End Sub

	''' <summary>
	''' Load all data charts that have been saved to file
	''' </summary>
	''' <remarks></remarks>
	Private Sub LoadAllDataCharts()

		' Make sure the directory exists
		If Directory.Exists(DataLogger.Charts.SaveLocation) = False Then
			Directory.CreateDirectory(DataLogger.Charts.SaveLocation)
		End If

		' Go through directory and load charts
		Dim files = Directory.GetFiles(DataLogger.Charts.SaveLocation, "*.xml")
		For Each file In files

			DataLogger.Charts.Add(DataChart.FromFile(file))

		Next


	End Sub

	Private Sub OnChartUpdating(ByVal sender As DataChart, ByVal e As EventArgs)

		If Me.InvokeRequired Then
			Me.Invoke(New MethodInvoker(Sub()
											OnChartUpdating(sender, e)
										End Sub))
			Return
		End If

		statusProgressBar.Visible = True

	End Sub

	Private Sub OnChartDataReady(ByVal sender As DataChart, ByVal e As DataChart.DataReadyEventArgs)

		If Me.InvokeRequired Then
			Me.Invoke(New MethodInvoker(Sub()
											OnChartDataReady(sender, e)
										End Sub))
			Return
		End If

		statusProgressBar.Visible = False

	End Sub

End Class
