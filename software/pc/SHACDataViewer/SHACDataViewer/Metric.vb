﻿Imports System.Runtime.Serialization
Imports System.ComponentModel
Imports System.Drawing.Design

<DataContractAttribute()>
<Editor(GetType(PropertyGridMetricPicker), GetType(UITypeEditor))>
Public Class Metric

	''' <summary>
	''' Name of the system in this metric
	''' </summary>
	''' <remarks></remarks>
	<DataMember()> _
	Public ReadOnly System As String

	''' <summary>
	''' Name of the sensor in this metric
	''' </summary>
	''' <remarks></remarks>
	<DataMember()> _
	Public ReadOnly Sensor As String

	''' <summary>
	''' Gets the key or string ID for this metric.
	''' </summary>
	Public ReadOnly Property Key() As String
		Get
			Return System & "." & Sensor
		End Get
	End Property


	''' <summary>
	''' Parameterless constructor for dumb xmlserializer
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()

		System = ""
		Sensor = ""

	End Sub

	''' <summary>
	''' Constructor
	''' </summary>
	''' <param name="systemName"></param>
	''' <param name="sensorName"></param>
	''' <remarks></remarks>
	Public Sub New(ByVal systemName As String, ByVal sensorName As String)

		System = systemName
		Sensor = sensorName

	End Sub

	Public Overrides Function ToString() As String

		Return Me.Key

	End Function

	Public Overrides Function Equals(ByVal obj As Object) As Boolean

		If (TypeOf obj Is Metric) = False Then
			Return False
		End If

		Dim m As Metric = obj
		If m.Key = Me.Key Then
			Return True
		End If

		Return False

	End Function

	Public Overrides Function GetHashCode() As Integer
		Return Me.Key.GetHashCode()
	End Function

End Class
