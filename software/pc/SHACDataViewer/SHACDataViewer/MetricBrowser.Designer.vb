﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MetricBrowser
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.metricTree = New System.Windows.Forms.TreeView()
		Me.treeContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.addMetricMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
		Me.collapseAllMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.expandAllMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Panel6 = New System.Windows.Forms.Panel()
		Me.filterButton = New System.Windows.Forms.Button()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.sensorFilterTextbox = New System.Windows.Forms.TextBox()
		Me.sensorFilterLabel = New System.Windows.Forms.Label()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.systemFilterTextbox = New System.Windows.Forms.TextBox()
		Me.systemFilterLabel = New System.Windows.Forms.Label()
		Me.filterPanelLabel = New System.Windows.Forms.Label()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.treeContextMenu.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.Panel6.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.Panel2.SuspendLayout()
		Me.SuspendLayout()
		'
		'metricTree
		'
		Me.metricTree.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.metricTree.ContextMenuStrip = Me.treeContextMenu
		Me.metricTree.Dock = System.Windows.Forms.DockStyle.Fill
		Me.metricTree.FullRowSelect = True
		Me.metricTree.HideSelection = False
		Me.metricTree.HotTracking = True
		Me.metricTree.Location = New System.Drawing.Point(0, 133)
		Me.metricTree.Name = "metricTree"
		Me.metricTree.Size = New System.Drawing.Size(345, 529)
		Me.metricTree.TabIndex = 3
		'
		'treeContextMenu
		'
		Me.treeContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.addMetricMenuItem, Me.ToolStripSeparator1, Me.collapseAllMenuItem, Me.expandAllMenuItem})
		Me.treeContextMenu.Name = "treeContextMenu"
		Me.treeContextMenu.Size = New System.Drawing.Size(137, 76)
		'
		'addMetricMenuItem
		'
		Me.addMetricMenuItem.Name = "addMetricMenuItem"
		Me.addMetricMenuItem.Size = New System.Drawing.Size(136, 22)
		Me.addMetricMenuItem.Text = "Add Metric"
		'
		'ToolStripSeparator1
		'
		Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
		Me.ToolStripSeparator1.Size = New System.Drawing.Size(133, 6)
		'
		'collapseAllMenuItem
		'
		Me.collapseAllMenuItem.Name = "collapseAllMenuItem"
		Me.collapseAllMenuItem.Size = New System.Drawing.Size(136, 22)
		Me.collapseAllMenuItem.Text = "Collapse All"
		'
		'expandAllMenuItem
		'
		Me.expandAllMenuItem.Name = "expandAllMenuItem"
		Me.expandAllMenuItem.Size = New System.Drawing.Size(136, 22)
		Me.expandAllMenuItem.Text = "Expand All"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Panel6)
		Me.Panel1.Controls.Add(Me.Panel3)
		Me.Panel1.Controls.Add(Me.Panel2)
		Me.Panel1.Controls.Add(Me.filterPanelLabel)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel1.Location = New System.Drawing.Point(0, 0)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(345, 128)
		Me.Panel1.TabIndex = 4
		'
		'Panel6
		'
		Me.Panel6.Controls.Add(Me.filterButton)
		Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel6.Location = New System.Drawing.Point(0, 96)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Padding = New System.Windows.Forms.Padding(7, 0, 7, 5)
		Me.Panel6.Size = New System.Drawing.Size(345, 32)
		Me.Panel6.TabIndex = 3
		'
		'filterButton
		'
		Me.filterButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.filterButton.Location = New System.Drawing.Point(235, 0)
		Me.filterButton.Name = "filterButton"
		Me.filterButton.Size = New System.Drawing.Size(103, 27)
		Me.filterButton.TabIndex = 4
		Me.filterButton.Text = "Filter"
		Me.filterButton.UseVisualStyleBackColor = True
		'
		'Panel3
		'
		Me.Panel3.AutoSize = True
		Me.Panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.Panel3.Controls.Add(Me.sensorFilterTextbox)
		Me.Panel3.Controls.Add(Me.sensorFilterLabel)
		Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel3.Location = New System.Drawing.Point(0, 64)
		Me.Panel3.MinimumSize = New System.Drawing.Size(0, 32)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Padding = New System.Windows.Forms.Padding(7, 0, 7, 7)
		Me.Panel3.Size = New System.Drawing.Size(345, 32)
		Me.Panel3.TabIndex = 2
		'
		'sensorFilterTextbox
		'
		Me.sensorFilterTextbox.Dock = System.Windows.Forms.DockStyle.Fill
		Me.sensorFilterTextbox.Location = New System.Drawing.Point(68, 0)
		Me.sensorFilterTextbox.Name = "sensorFilterTextbox"
		Me.sensorFilterTextbox.Size = New System.Drawing.Size(270, 25)
		Me.sensorFilterTextbox.TabIndex = 1
		'
		'sensorFilterLabel
		'
		Me.sensorFilterLabel.Dock = System.Windows.Forms.DockStyle.Left
		Me.sensorFilterLabel.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.sensorFilterLabel.Location = New System.Drawing.Point(7, 0)
		Me.sensorFilterLabel.Name = "sensorFilterLabel"
		Me.sensorFilterLabel.Size = New System.Drawing.Size(61, 25)
		Me.sensorFilterLabel.TabIndex = 0
		Me.sensorFilterLabel.Text = "Sensor"
		Me.sensorFilterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'Panel2
		'
		Me.Panel2.AutoSize = True
		Me.Panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.Panel2.Controls.Add(Me.systemFilterTextbox)
		Me.Panel2.Controls.Add(Me.systemFilterLabel)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel2.Location = New System.Drawing.Point(0, 25)
		Me.Panel2.MinimumSize = New System.Drawing.Size(0, 39)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Padding = New System.Windows.Forms.Padding(7, 7, 7, 10)
		Me.Panel2.Size = New System.Drawing.Size(345, 39)
		Me.Panel2.TabIndex = 1
		'
		'systemFilterTextbox
		'
		Me.systemFilterTextbox.Dock = System.Windows.Forms.DockStyle.Fill
		Me.systemFilterTextbox.Location = New System.Drawing.Point(68, 7)
		Me.systemFilterTextbox.Name = "systemFilterTextbox"
		Me.systemFilterTextbox.Size = New System.Drawing.Size(270, 25)
		Me.systemFilterTextbox.TabIndex = 1
		'
		'systemFilterLabel
		'
		Me.systemFilterLabel.Dock = System.Windows.Forms.DockStyle.Left
		Me.systemFilterLabel.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.systemFilterLabel.Location = New System.Drawing.Point(7, 7)
		Me.systemFilterLabel.Name = "systemFilterLabel"
		Me.systemFilterLabel.Size = New System.Drawing.Size(61, 22)
		Me.systemFilterLabel.TabIndex = 0
		Me.systemFilterLabel.Text = "System"
		Me.systemFilterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'filterPanelLabel
		'
		Me.filterPanelLabel.BackColor = System.Drawing.Color.WhiteSmoke
		Me.filterPanelLabel.Dock = System.Windows.Forms.DockStyle.Top
		Me.filterPanelLabel.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.filterPanelLabel.ForeColor = System.Drawing.Color.DimGray
		Me.filterPanelLabel.Location = New System.Drawing.Point(0, 0)
		Me.filterPanelLabel.Name = "filterPanelLabel"
		Me.filterPanelLabel.Size = New System.Drawing.Size(345, 25)
		Me.filterPanelLabel.TabIndex = 0
		Me.filterPanelLabel.Text = "Metrics"
		'
		'Panel4
		'
		Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
		Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel4.Location = New System.Drawing.Point(0, 128)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(345, 5)
		Me.Panel4.TabIndex = 5
		'
		'MetricBrowser
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.White
		Me.Controls.Add(Me.metricTree)
		Me.Controls.Add(Me.Panel4)
		Me.Controls.Add(Me.Panel1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "MetricBrowser"
		Me.Size = New System.Drawing.Size(345, 662)
		Me.treeContextMenu.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.Panel6.ResumeLayout(False)
		Me.Panel3.ResumeLayout(False)
		Me.Panel3.PerformLayout()
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents metricTree As System.Windows.Forms.TreeView
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents filterPanelLabel As System.Windows.Forms.Label
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents sensorFilterTextbox As System.Windows.Forms.TextBox
	Friend WithEvents sensorFilterLabel As System.Windows.Forms.Label
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents systemFilterTextbox As System.Windows.Forms.TextBox
	Friend WithEvents systemFilterLabel As System.Windows.Forms.Label
	Friend WithEvents Panel4 As System.Windows.Forms.Panel
	Friend WithEvents Panel6 As System.Windows.Forms.Panel
	Friend WithEvents filterButton As System.Windows.Forms.Button
    Friend WithEvents treeContextMenu As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents collapseAllMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents expandAllMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents addMetricMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator

End Class
