﻿Imports System.Collections.ObjectModel

Public Class MetricBrowser

    Public Event MetricAddRequest(ByVal sender As Object, ByVal e As MetricSelectionEventArgs)

    ''' <summary>
    ''' Update metric tree based on system and sensor name filter strings. 
    ''' Wildcard = %
    ''' </summary>
    ''' <param name="systemFilter"></param>
    ''' <param name="sensorFilter"></param>
    ''' <remarks></remarks>
    Private Sub UpdateMetricTree(ByVal systemFilter As String, ByVal sensorFilter As String)

        ' Make sure filters aren't just empty
        If systemFilter = "" Then
            systemFilter = "%"
        End If
        If sensorFilter = "" Then
            sensorFilter = "%"
        End If

        ' Run query
        Dim data = DataLogger.Database.ExecuteSelect(String.Format("SELECT id, system,sensor FROM datapoints " &
           "WHERE system LIKE '{0}' AND " &
           "sensor LIKE '{1}' " &
           "GROUP BY system, sensor", systemFilter, sensorFilter))

        ' Tell tree that we are going to add a bunch of nodes to it
        metricTree.Nodes.Clear()
        metricTree.BeginUpdate()

        ' Go through rows and fill metric tree
        For i As Integer = 0 To data.Count - 1

            ' Get current row data
            Dim rowData = data.Item(i)

            ' Add system node if it doesn't exist
            If metricTree.Nodes.ContainsKey(rowData.Item("system")) = False Then
                metricTree.Nodes.Add(rowData.Item("system"), rowData.Item("system"))
            End If

            ' Get system node
            Dim systemNode As TreeNode = metricTree.Nodes.Item(rowData.Item("system"))

            ' Add to system node
            systemNode.Nodes.Add(rowData.Item("sensor"), rowData.Item("sensor"))

        Next

        ' Tell tree we are done
        metricTree.EndUpdate()

    End Sub

    ''' <summary>
    ''' Handle filter button being clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub filterButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filterButton.Click

        Me.UpdateMetricTree(systemFilterTextbox.Text, sensorFilterTextbox.Text)
        metricTree.Focus()

    End Sub

    ''' <summary>
    ''' For convenience, trigger filtering when user presses enter in any of the filter textboxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub systemFilterTextbox_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles systemFilterTextbox.KeyDown, sensorFilterTextbox.KeyDown

        If e.KeyCode = Keys.Enter Then
            Me.UpdateMetricTree(systemFilterTextbox.Text, sensorFilterTextbox.Text)
            metricTree.Focus()

        End If

    End Sub

    ''' <summary>
    ''' Collapse the entire metric tree
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub collapseAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles collapseAllMenuItem.Click

        metricTree.CollapseAll()
        metricTree.Focus()
        metricTree.TopNode = metricTree.SelectedNode

    End Sub

    ''' <summary>
    ''' Expand all the nodes in the metric tree
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub expandAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles expandAllMenuItem.Click

        metricTree.ExpandAll()
        metricTree.Focus()
        metricTree.TopNode = metricTree.SelectedNode

    End Sub

    ''' <summary>
    ''' Select nodes in the metric tree when any mouse button is clicked, 
    ''' not just the left button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub metricTree_NodeMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles metricTree.NodeMouseClick

        metricTree.SelectedNode = e.Node

    End Sub

    ''' <summary>
    ''' Handle add metrics button being clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub addMetricMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addMetricMenuItem.Click

        ' Currently selected node
        Dim node = metricTree.SelectedNode

        ' List of metrics we will be raising event with
        Dim metricList = New List(Of Metric)

        ' If it's a system node, we add all sensors underneath
        If node.Nodes.Count > 0 Then

            ' Make sure user wants to add all the sensors in this system
            If MessageBox.Show("Add all metrics in system '" & node.Text & "'?", "Add", MessageBoxButtons.YesNo) = DialogResult.No Then
                Return

            End If

            ' Guess so
            For Each n As TreeNode In node.Nodes
                metricList.Add(New Metric(node.Text, n.Text))
            Next

        Else
            ' Selected single sensor
            metricList.Add(New Metric(node.Parent.Text, node.Text))

        End If

        ' Raise event
        RaiseEvent MetricAddRequest(Me, New MetricSelectionEventArgs(metricList))

    End Sub

    ''' <summary>
    ''' Event arguments for metric selection (add/remove)
    ''' </summary>
    ''' <remarks></remarks>
    Public Class MetricSelectionEventArgs

        ''' <summary>
        ''' List of metrics in this selection event
        ''' </summary>
        ''' <remarks></remarks>
        Public ReadOnly Metrics As ReadOnlyCollection(Of Metric)

        ''' <summary>
        ''' Constructor
        ''' </summary>
        ''' <param name="metricList"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal metricList As List(Of Metric))

            Metrics = metricList.AsReadOnly()

        End Sub

    End Class

End Class
