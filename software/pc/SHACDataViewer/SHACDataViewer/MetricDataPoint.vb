﻿''' <summary>
''' Holds a single data point for a given metric
''' </summary>
''' <remarks></remarks>
Public Structure MetricDataPoint

    Public ReadOnly Id As Integer
    Public ReadOnly System As String
    Public ReadOnly Sensor As String
    Public ReadOnly Value As Double
    Public ReadOnly Timestamp As DateTime

    ''' <summary>
    ''' Simple constructor
    ''' </summary>
    ''' <param name="_id"></param>
    ''' <param name="_system"></param>
    ''' <param name="_sensor"></param>
    ''' <param name="_value"></param>
    ''' <param name="_timestamp"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal _id As Integer, ByVal _system As String, ByVal _sensor As String, ByVal _value As Double, ByVal _timestamp As DateTime)

        Id = _id
        System = _system
        Sensor = _sensor
        Value = _value
        Timestamp = _timestamp

    End Sub

End Structure

