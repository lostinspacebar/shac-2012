﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MetricPickerDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.metricSelect = New System.Windows.Forms.ListBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.okButton = New System.Windows.Forms.Button()
		Me.cancelButton = New System.Windows.Forms.Button()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'metricSelect
		'
		Me.metricSelect.Dock = System.Windows.Forms.DockStyle.Fill
		Me.metricSelect.FormattingEnabled = True
		Me.metricSelect.ItemHeight = 17
		Me.metricSelect.Location = New System.Drawing.Point(0, 0)
		Me.metricSelect.Name = "metricSelect"
		Me.metricSelect.Size = New System.Drawing.Size(262, 263)
		Me.metricSelect.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cancelButton)
		Me.Panel1.Controls.Add(Me.okButton)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 263)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Padding = New System.Windows.Forms.Padding(5)
		Me.Panel1.Size = New System.Drawing.Size(262, 40)
		Me.Panel1.TabIndex = 1
		'
		'okButton
		'
		Me.okButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.okButton.Location = New System.Drawing.Point(182, 5)
		Me.okButton.Name = "okButton"
		Me.okButton.Size = New System.Drawing.Size(75, 30)
		Me.okButton.TabIndex = 0
		Me.okButton.Text = "OK"
		Me.okButton.UseVisualStyleBackColor = True
		'
		'cancelButton
		'
		Me.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cancelButton.Dock = System.Windows.Forms.DockStyle.Right
		Me.cancelButton.Location = New System.Drawing.Point(107, 5)
		Me.cancelButton.Name = "cancelButton"
		Me.cancelButton.Size = New System.Drawing.Size(75, 30)
		Me.cancelButton.TabIndex = 1
		Me.cancelButton.Text = "Cancel"
		Me.cancelButton.UseVisualStyleBackColor = True
		'
		'MetricPickerDialog
		'
		Me.AcceptButton = Me.okButton
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
		Me.cancelButton = Me.cancelButton
		Me.ClientSize = New System.Drawing.Size(262, 303)
		Me.ControlBox = False
		Me.Controls.Add(Me.metricSelect)
		Me.Controls.Add(Me.Panel1)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "MetricPickerDialog"
		Me.Text = "MetricPickerDialog"
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents metricSelect As System.Windows.Forms.ListBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cancelButton As System.Windows.Forms.Button
	Friend WithEvents okButton As System.Windows.Forms.Button
End Class
