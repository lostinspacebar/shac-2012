﻿Imports System.Drawing.Design
Imports System.Windows.Forms.Design

Public Class MetricPickerDialog

	''' <summary>
	''' List of metrics to choose from
	''' </summary>
	Public Metrics As Metric()

	''' <summary>
	''' Gets or sets the selected metric in the dialog
	''' </summary>
	Public Property SelectedMetric As Metric
		Get
			If metricSelect.SelectedIndices.Count = 1 Then
				Return Metrics(metricSelect.SelectedIndex)
			End If
		End Get
		Set(ByVal value As Metric)
			If value IsNot Nothing Then
				For i As Integer = 0 To Metrics.Length - 1
					If Metrics(i).Key = value.Key Then
						metricSelect.SelectedIndex = i
					End If
				Next
			End If
		End Set
	End Property

		''' <summary>
		''' Constructor
		''' </summary>
		''' <remarks></remarks>
	Public Sub New(ByVal _metrics As Metric())

		' This call is required by the designer.
		InitializeComponent()

		' Add any initialization after the InitializeComponent() call.
		Metrics = _metrics

		For Each m In Metrics
			metricSelect.Items.Add(m.Key)
		Next

		If Metrics.Length > 0 Then
			metricSelect.SelectedIndex = 0
		End If

	End Sub

	Private Sub okButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles okButton.Click

		Me.DialogResult = Windows.Forms.DialogResult.OK
		Me.Hide()

	End Sub

	Private Sub cancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelButton.Click

		Me.DialogResult = DialogResult.Cancel
		Me.Hide()

	End Sub

	Private Sub metricSelect_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles metricSelect.MouseDoubleClick

		If e.Button = MouseButtons.Left Then

			Me.DialogResult = Windows.Forms.DialogResult.OK
			Me.Hide()

		End If

	End Sub

End Class

Public Class PropertyGridMetricPicker
	Inherits UITypeEditor

	Public Shared Metrics As Metric()

	Public Overrides Function GetEditStyle(ByVal context As System.ComponentModel.ITypeDescriptorContext) As System.Drawing.Design.UITypeEditorEditStyle

		Return UITypeEditorEditStyle.Modal

	End Function

	Public Overrides Function EditValue(ByVal context As System.ComponentModel.ITypeDescriptorContext, ByVal provider As System.IServiceProvider, ByVal value As Object) As Object

		If ((context IsNot Nothing) And (context.Instance IsNot Nothing) And (provider IsNot Nothing)) Then

			Dim svc As IWindowsFormsEditorService = provider.GetService(GetType(IWindowsFormsEditorService))
			Dim picker = New MetricPickerDialog(Metrics)
			picker.SelectedMetric = value
			If svc.ShowDialog(picker) = DialogResult.OK Then
				Return picker.SelectedMetric
			Else
				Return value
			End If

		End If

	End Function

End Class