﻿Public Class ReadOnlyDictionary(Of K, V)

	''' <summary>
	''' Indexer
	''' </summary>
	Default Public ReadOnly Property Item(ByVal index As K) As V
		Get
			Return _secretDictionary.Item(index)
		End Get
	End Property

	''' <summary>
	''' Actual dictionary with data
	''' </summary>
	''' <remarks></remarks>
	Private _secretDictionary As Dictionary(Of K, V)

	''' <summary>
	''' Constructor
	''' </summary>
	''' <param name="dict"></param>
	''' <remarks></remarks>
	Public Sub New(ByVal dict As Dictionary(Of K, V))
		_secretDictionary = dict
	End Sub

End Class
