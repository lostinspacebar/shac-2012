﻿Public Class SeriesTreeView
	Inherits TreeView

	Protected Overrides Sub OnDrawNode(ByVal e As System.Windows.Forms.DrawTreeNodeEventArgs)

		Dim seriesNode As SeriesTreeNode = e.Node
		e.Graphics.FillRectangle(seriesNode.ColorBrush, New Rectangle(0, 0, 5, e.Bounds.Height))

		MyBase.OnDrawNode(e)

	End Sub

End Class

Public Class SeriesTreeNode
	Inherits TreeNode

	Private _brush As SolidBrush = Brushes.Black
	Public ReadOnly Property ColorBrush() As SolidBrush
		Get
			Return _brush
		End Get
	End Property

	Public Property Color() As Color
		Get
			Return _brush.Color
		End Get
		Set(ByVal value As Color)
			_brush.Color = value
		End Set
	End Property



End Class
