﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TimespanPicker
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.daysSelect = New System.Windows.Forms.NumericUpDown()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.hrsSelect = New System.Windows.Forms.NumericUpDown()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.minSelect = New System.Windows.Forms.NumericUpDown()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.secSelect = New System.Windows.Forms.NumericUpDown()
		CType(Me.daysSelect, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.hrsSelect, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.minSelect, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.secSelect, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'daysSelect
		'
		Me.daysSelect.Dock = System.Windows.Forms.DockStyle.Left
		Me.daysSelect.Location = New System.Drawing.Point(0, 0)
		Me.daysSelect.Name = "daysSelect"
		Me.daysSelect.Size = New System.Drawing.Size(40, 25)
		Me.daysSelect.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.Dock = System.Windows.Forms.DockStyle.Left
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(40, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
		Me.Label1.Size = New System.Drawing.Size(17, 29)
		Me.Label1.TabIndex = 1
		Me.Label1.Text = "d"
		'
		'Label2
		'
		Me.Label2.Dock = System.Windows.Forms.DockStyle.Left
		Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(97, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
		Me.Label2.Size = New System.Drawing.Size(28, 29)
		Me.Label2.TabIndex = 3
		Me.Label2.Text = "hrs"
		'
		'hrsSelect
		'
		Me.hrsSelect.Dock = System.Windows.Forms.DockStyle.Left
		Me.hrsSelect.Location = New System.Drawing.Point(57, 0)
		Me.hrsSelect.Name = "hrsSelect"
		Me.hrsSelect.Size = New System.Drawing.Size(40, 25)
		Me.hrsSelect.TabIndex = 2
		'
		'Label3
		'
		Me.Label3.Dock = System.Windows.Forms.DockStyle.Left
		Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(165, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
		Me.Label3.Size = New System.Drawing.Size(29, 29)
		Me.Label3.TabIndex = 5
		Me.Label3.Text = "min"
		'
		'minSelect
		'
		Me.minSelect.Dock = System.Windows.Forms.DockStyle.Left
		Me.minSelect.Location = New System.Drawing.Point(125, 0)
		Me.minSelect.Name = "minSelect"
		Me.minSelect.Size = New System.Drawing.Size(40, 25)
		Me.minSelect.TabIndex = 4
		'
		'Label4
		'
		Me.Label4.Dock = System.Windows.Forms.DockStyle.Left
		Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(234, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
		Me.Label4.Size = New System.Drawing.Size(28, 29)
		Me.Label4.TabIndex = 7
		Me.Label4.Text = "sec"
		'
		'secSelect
		'
		Me.secSelect.Dock = System.Windows.Forms.DockStyle.Left
		Me.secSelect.Location = New System.Drawing.Point(194, 0)
		Me.secSelect.Name = "secSelect"
		Me.secSelect.Size = New System.Drawing.Size(40, 25)
		Me.secSelect.TabIndex = 6
		'
		'TimespanPicker
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.secSelect)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.minSelect)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.hrsSelect)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.daysSelect)
		Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
		Me.Name = "TimespanPicker"
		Me.Size = New System.Drawing.Size(341, 29)
		CType(Me.daysSelect, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.hrsSelect, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.minSelect, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.secSelect, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents daysSelect As System.Windows.Forms.NumericUpDown
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents hrsSelect As System.Windows.Forms.NumericUpDown
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents minSelect As System.Windows.Forms.NumericUpDown
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents secSelect As System.Windows.Forms.NumericUpDown

End Class
