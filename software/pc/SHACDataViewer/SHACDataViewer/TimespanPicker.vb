﻿Public Class TimespanPicker

	''' <summary>
	''' Event raised when the value for the timespan picker has changed
	''' </summary>
	Public Event ValueChanged(ByVal sender As Object, ByVal e As EventArgs)

	''' <summary>
	''' Current value in the timespan picker
	''' </summary>
	Private _timespan As TimeSpan = New TimeSpan(0, 0, 0, 0)
	Public Property Value() As TimeSpan

		Get
			Return _timespan
		End Get

		Set(ByVal value As TimeSpan)
			If value <> _timespan Then
				' Save new value
				_timespan = value

				' Update UI for new value
				isUpdating = True
				daysSelect.Value = _timespan.Days
				hrsSelect.Value = _timespan.Hours
				minSelect.Value = _timespan.Minutes
				secSelect.Value = _timespan.Seconds
				isUpdating = False

				' Raise event
				RaiseEvent ValueChanged(Me, EventArgs.Empty)

			End If

		End Set

	End Property

	Private isUpdating As Boolean = False

	''' <summary>
	''' 
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub daysSelect_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles secSelect.ValueChanged, minSelect.ValueChanged, hrsSelect.ValueChanged, daysSelect.ValueChanged

		If isUpdating Then
			Return
		End If

		' Update timespan
		_timespan = New TimeSpan(daysSelect.Value, hrsSelect.Value, minSelect.Value, secSelect.Value)

		' Raise event
		RaiseEvent ValueChanged(Me, EventArgs.Empty)

	End Sub

End Class
